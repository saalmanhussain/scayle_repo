<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Services extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('cart');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }

    public function index() {
        
        $data = array(
            'user_ip' => $this->input->ip_address(),
            'created' => date("Y-m-d")
        );

        $table = 'current_users';
        $query = $this->query->insert_query($table, $data);

        $data['site_Info'] = $this->custom->site_Info();
        $data['msg'] = "";
        $data['menu'] = $this->query->get_menu();
	   
	    $data['slider']      = $this->query->get_all_active_slider();
	    $data['OurPartners'] = $this->query->get_our_partners();

     	$data['Services'] = $this->query->get_all_active_page(4);
        $data['Services_details'] = $this->query->get_all_active_page_details(4);

        $data['OurServices'] = $this->query->get_all_active_services();

		
        $this->load->view('services', $data);
    }


}
