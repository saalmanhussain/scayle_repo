<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('cart');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }

    public function index() {

        $data = array(
            'user_ip' => $this->input->ip_address(),
            'created' => date("Y-m-d")
        );

        $table = 'current_users';
        $query = $this->query->insert_query($table, $data);

        $data['site_Info'] = $this->custom->site_Info();
        $data['msg'] = "";
        $data['menu'] = $this->query->get_menu();

        $data['slider'] = $this->query->get_all_active_slider();
        $data['OurPartners'] = $this->query->get_our_partners();
        $data['OurServices'] = $this->query->get_all_active_services();

        $data['Home'] = $this->query->get_all_active_page(1);
        $data['Home_details'] = $this->query->get_all_active_page_details(1);

        $this->load->view('home', $data);
    }

    public function header_cart_view($partner) {

        $cart_check = $this->cart->contents();
        
        $count = 0;
        $string = "";
        $qty = 0;
        $total = 0;
        
        foreach ($cart_check as $row) {
            
            if($row['options']['user_id'] == $partner)
            { 
                $rowid = "'" . $row['rowid'] . "'";
                $p_id = "'" . $row['options']['user_id'] . "'";
                $qty += $row['qty'];
                $total += $row['qty'] * $row['price'];
                
                $string .=  '<tr>
                             <td><small>'.$row['name'].'</small></td>
                             <td><small>'.$row['qty'].'</small></td>
                             <td><small>Rs.</small>'.$row['qty'] * $row['price'].'</td>
                             <td> <a class="button small grey square" onclick="subtract_cart_item(' . $rowid . ','. $p_id .')">-</a> </td>
                             <td> <a class="button small grey square" onclick="add_cart_item(' . $rowid . ','. $p_id .')">+</a> </td>
                             <td> <a class="button small red square" onclick="remove_cart_item(' . $rowid . ','. $p_id .')">x</a> </td>
                             </tr>';
                $count = 1;
            }
            
        }
        
        if($count == 0)
        {
            $string .=  "<tr><td colspan='6'><small>No Product in cart</small></td></tr>";
        }
        else
        {
            $string .= '<tr>
                             <td><small>Total</small></td>
                             <td><small>' . $qty . '</small></td>
                             <td><small>Rs.</small>' . $total . '</td>
                             <td></td>
                             <td></td>
                             <td></td>
                             </tr>';
        }

        echo $string;
        
        //echo $cart_check1;
    }

    public function empty_cart() {
        $this->cart->destroy();
        redirect(base_url());
    }
    
    public function remove_cart_item()
    {
        $pId = $this->input->post('pId');
        $partner_id = $this->input->post('partner_id');
        
        //echo $carId;

        $data = array(
            'rowid' => $pId,
            'qty' => 0
        );

        $add = $this->cart->update($data);

        if ($add) {
            $this->header_cart_view($partner_id);
        } else {
            echo "0";
        }
    }
    
    public function subtract_cart_item()
    {
        $pId = $this->input->post('pId');
        $partner_id = $this->input->post('partner_id');
        
        //echo $carId;
        
        foreach ($this->cart->contents() as $items) {

            if ($items['rowid'] == $pId) {
                $Qty = $items['qty'];
            }
        }
        
        $data = array(
            'rowid' => $pId,
            'qty' => $Qty-1
        );

        $add = $this->cart->update($data);

        if ($add) {
            $this->header_cart_view($partner_id);
        } else {
            echo "0";
        }
    }
    
    public function add_cart_item()
    {
        $pId = $this->input->post('pId');
        $partner_id = $this->input->post('partner_id');
        
        //echo $carId;
        
        foreach ($this->cart->contents() as $items) {

            if ($items['rowid'] == $pId) {
                $Qty = $items['qty'];
            }
        }
        
        $data = array(
            'rowid' => $pId,
            'qty' => $Qty+1
        );

        $add = $this->cart->update($data);

        if ($add) {
            $this->header_cart_view($partner_id);
        } else {
            echo "0";
        }
    }
    
    public function add_cart() {

        $id     = $this->input->post('id');
        $qty     = $this->input->post('qty');
        $price   = $this->input->post('price');
        $name    = $this->input->post('name');
        $user_id = $this->input->post('user_id');
        $count   = 0;
        
        if($price == "")
        {
            $price = 1;
        }
        
        foreach ($this->cart->contents() as $items) {

            if ($items['id'] == $id) {
                $count    = 1;
                $Qty      = $items['qty'] + $qty;
                $row_id   = $items['rowid'];
            }
        }

        if ($count == 0) {
            $insert_data = array(
                'id'      => $id,
                'price'   => $price,
                'name'    => $name,
                'qty'     => $qty,
                'options' => array('user_id' => $user_id)
            );

            $add = $this->cart->insert($insert_data);
            
        } else {
            $data = array(
                'rowid' => $row_id,
                'qty'   => $Qty
            );

            $add = $this->cart->update($data);
        }


        if ($add) {
            $this->header_cart_view($user_id);
        } else {
            echo "-1";
        }
    }
    
    public function checkout_form() {
        
        $checkout_name    = $this->input->post('checkout_name');
        $checkout_email   = $this->input->post('checkout_email');
        $checkout_phone   = $this->input->post('checkout_phone');
        $partner_id		  = $this->input->post('partner_id');
        $checkout_address = $this->input->post('checkout_address');
        
        $data = array(
            'email' 	 => $checkout_email,
            'phone' 	 => $checkout_phone,
            'name' 	=> $checkout_name,
            'address' 	 => $checkout_address,
            'partnerId'  => $partner_id,
            'status' 	 => 1,
            'created' 	 => date("Y-m-d"),
            'view_order' => 0,
            'send_email' => 0
        );

        $table = 'customer_order';
        $query = $this->query->insert_query($table, $data);
        
        if($query['query'])
        {
            $orderId = $query['CreatedID'];
            
            $cart_check = $this->cart->contents();

            foreach ($cart_check as $row) {

                if ($row['options']['user_id'] == $partner_id) {
                    
                    $data = array(
                        'item' => $row['name'],
                        'qty' => $row['qty'],
                        'price' => $row['price'],
                        'orderId' => $orderId
                    );

                    $table = 'order_details';
                    $query = $this->query->insert_query($table, $data);
                    
                }
            }
            
            $emailmsg = '';

            $emailmsg .= '<div style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#000000">';
            $emailmsg .= '<div style="width:680px"><a href="' . base_url() . '" title="Your Store" target="_blank"><img class="CToWUd" src="' . base_url() . 'assets/images/logo.png" alt="Your Store" style="margin-bottom:20px;border:none"></a>';
            $emailmsg .= '<p style="margin-top:0px;margin-bottom:20px"> Your order has been received and will be processed.</p>';
            $emailmsg .= '<p style="margin-top:0px;margin-bottom:20px">&nbsp;</p>';
            $emailmsg .= '<table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">';
            $emailmsg .= '<thead>';
            $emailmsg .= '<tr>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222" colspan="2">Order Details</td>';
            $emailmsg .= '</tr>';
            $emailmsg .= '</thead>';
            $emailmsg .= '<tbody>';
            $emailmsg .= '<tr>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"><b>Order ID:</b> ' . $orderId . '<br>';
            $emailmsg .= '<b>Date Added:</b> ' . date("d/m/Y") . '<br></td>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"><b>User:</b> <a href="" target="_blank">' . $checkout_name . '</a><br>';
            $emailmsg .= '<b>Email:</b> <a href="mailto:' . $checkout_email . '" target="_blank">' . $checkout_email . '</a><br>';
            $emailmsg .= '</tr>';
            $emailmsg .= '</tbody>';
            $emailmsg .= '</table>';
            $emailmsg .= '<table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">';
            $emailmsg .= '<thead>';
            $emailmsg .= '<tr>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Ship To Person</td>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Shipping Address</td>';
            $emailmsg .= ' </tr>';
            $emailmsg .= '</thead>';
            $emailmsg .= '<tbody>';
            $emailmsg .= '<tr>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"><strong>Name :</strong> ' . $checkout_name . '<br> <strong>Email :</strong>' . $checkout_email . '<br><strong>Contact No :</strong>' . $checkout_phone . '<br></td>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">';

            $emailmsg .= '</td>';
            $emailmsg .= '</tr>';
            $emailmsg .= '</tbody>';
            $emailmsg .= '</table>';
            $emailmsg .= '<table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">';
            $emailmsg .= '<thead>';
            $emailmsg .= '<tr>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Product</td>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">Quantity</td>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">Price</td>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">Total</td>';
            $emailmsg .= '</tr>';
            $emailmsg .= '</thead>';
            $emailmsg .= '<tbody>';

            $i = 0;
            $total = 0;
            foreach ($cart_check as $item) {

                if ($item['options']['user_id'] == $partner_id) {
                    $total += $item['price'];

                    $name = $item['id'];
                    $p_name = $name;

                    $emailmsg .= '<tr>';
                    $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">' . $p_name . '</td>';
                    $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">' . $item['qty'] . '</td>';
                    $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">' . $item['price'] . '</td>';
                    $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">' . $item['price'] * $item['qty'] . '</td>';
                    $emailmsg .= '</tr>';
                }
            }
            $emailmsg .= '</tbody>';
            $emailmsg .= '<tfoot>';
            $emailmsg .= '<tr>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px" colspan="3"><b>Total:</b></td>';
            $emailmsg .= '<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">' . $total . '</td>';
            $emailmsg .= '</tr>';
            $emailmsg .= '</tfoot>';
            $emailmsg .= '</table>';
            $emailmsg .= '<p style="margin-top:0px;margin-bottom:20px">&nbsp;</p></div><div class="adL">';
            $emailmsg .= '</div></div>';

            $config['protocol'] = 'sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';

            $this->email->initialize($config);
            $this->email->from('info@infotwister.com', 'Info Twister');
            $this->email->to($checkout_email);
            $this->email->subject('Order Recived');

            $html = $emailmsg;
            $this->email->message($html);
            $this->email->send();

            echo "1";
        }
    }   

}
