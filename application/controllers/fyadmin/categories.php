<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }

    public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }

    public function index() {
        if ($this->session->userdata('Is_Login') == true) {
            $data['msg'] = "";
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['page_active'] = $this->get_view();
            $data['accounts'] = $this->query->get_all_account();
            $this->load->view('fyadmin/view_category', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function add_category() {
        if ($this->session->userdata('Is_Login') == true) {

            $data["msg"] = "";

            if (isset($_POST['add_category'])) {

                if ($_FILES['cat_image']['tmp_name']) {
                    $config['file_name'] = $_FILES['cat_image']['name'];
                    $config['upload_path'] = 'assets/upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('cat_image');
                    $Image1 = $this->upload->data();

                    $cat_image = $Image1['file_name'];
                } else {
                    $cat_image = '';
                }

                if ($cat_image != '') {

                    $data = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                        'image' => $cat_image,
                        'seo' => $this->custom->seo_Url($this->input->post('title')),
                        'status' => 1,
                        'created' => date("Y-m-d"),
                        'accountId_created' => $this->session->userdata('AccountId')
                    );

                    $check_category = $this->query->check_category($this->input->post('title'));

                    if (count($check_category) == 0) {
                        $table = 'categories';
                        $query = $this->query->insert_query($table, $data);

                        if ($query['query'] == 1) {
                            $this->session->set_userdata('Success', "Record Has Been Successfully Added...");
                            redirect('fyadmin/categories/add_category');
                        } else {
                            $this->session->set_userdata('Error', "Please Try agin...");
                            redirect('fyadmin/categories/add_category');
                        }
                    } else {
                        $this->session->set_userdata('Error', "Category Already Exist...");
                        redirect('fyadmin/categories/add_category');
                    }
                } else {
                    $this->session->set_userdata('Error', "Please Try Again");
                    redirect('fyadmin/categories/add_category');
                }
            }

            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);

            $data['page_active'] = $this->get_view();
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');

            $this->load->view('fyadmin/add_category', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function view_category() {
        if ($this->session->userdata('Is_Login') == true) {
            $data['msg'] = "";
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['page_active'] = $this->get_view();
            $data['accounts'] = $this->query->get_all_account();
            $this->load->view('fyadmin/view_category', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function view_category_Json() {
        $status = "";
        $categories = $this->query->get_all_categories();
        $count=1;
        foreach ($categories as $row) {

            $status .= "<a class='waves-effect waves-dark btn purple' title='Edit' href='" . base_url() . "fyadmin/categories/edit_category?id=".base64_encode($row->catID)."'>
                        <i class='mdi-editor-mode-edit'></i></a>";

            if ($row->status == 0) {
                $status .= "<a class='waves-effect waves-dark btn red' title='Enable' href='" . base_url() . "fyadmin/categories/activate_category?id=".base64_encode($row->catID)."'>
                        <i class='mdi-action-thumb-up'></i></a>";
            } else {
                $status .= "<a class='waves-effect waves-dark btn red' title='Disable' href='" . base_url() . "fyadmin/categories/delete_category?id=".base64_encode($row->catID)."'>
                        <i class='mdi-action-thumb-down'></i></a>";
            }
            
            $rows[0] = $count++;
            $rows[1] = $row->title;
            if(strlen($row->description) > 50)
            {
                $rows[2] = substr($row->description,0,55)."...";
            }
            else
            {
                $rows[2] = $row->description;
            }
            $rows[3] = "<img src='" . PATH . "upload/$row->image' width='100' />";
            $rows[4] = $status;
            $response['aaData'][] = $rows;
            $status = "";
        }

        echo json_encode($response, true);
    }

    public function delete_category() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = base64_decode($_GET['id']);

            $data = array(
                'status' => 0
            );

            $query = $this->query->update_query('categories', $data, 'catID', $id);

            if ($query == 1) {
                $this->session->set_userdata('Success', "Category Disabled Successfully");
                //$data['msg'] = "<p class='login-box-msg' style='color: green;'>Account Disabled Successfully</p>";
            } else {
                $this->session->set_userdata('Error', "Please Try Again");
                //$data['msg'] = "<p class='login-box-msg' style='color: red;'>Please Try agin</p>";
            }
            
            redirect(base_url().'fyadmin/categories/view_category');
            
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);

            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/view_category', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function activate_category() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = base64_decode($_GET['id']);

            $data = array(
                'status' => 1
            );

            $query = $this->query->update_query('categories', $data, 'catID', $id);

            if ($query == 1) {
                $this->session->set_userdata('Success', "Category Enabled Successfully");
                //$data['msg'] = "<p class='login-box-msg' style='color: green;'>Account Disabled Successfully</p>";
            } else {
                $this->session->set_userdata('Error', "Please Try Again");
                //$data['msg'] = "<p class='login-box-msg' style='color: red;'>Please Try agin</p>";
            }
            
            redirect(base_url().'fyadmin/categories/view_category');
            
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);

            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/view_category', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function edit_category() {
        
        
        
        if ($this->session->userdata('Is_Login') == true) {
            
            $id = base64_decode($_GET['id']);
            
            $data['msg'] = "";

            if (isset($_POST['edit_category'])) {

                if ($_FILES['cat_image']['tmp_name']) {
                    $config['file_name'] = $_FILES['cat_image']['name'];
                    $config['upload_path'] = 'assets/upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('cat_image');
                    $Image1 = $this->upload->data();

                    $cat_image = $Image1['file_name'];
                } else {
                    $cat_image = $this->input->post('cat_old_image');
                }

                if ($cat_image != '') {

                    $data = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                        'seo' => $this->custom->seo_Url($this->input->post('title')),
                        'image' => $cat_image,
                        'status' => 1,
                        'updated' => date("Y-m-d"),
                        'accountId_updated' => $this->session->userdata('AccountId')
                    );

                    $table = 'categories';
                    $cat_id = $this->input->post('catID');

                    $query = $this->query->update_query($table, $data, 'catID', $cat_id);

                    if ($query == 1) {
                        $this->session->set_userdata('Success', "Record Has Been Successfully Updated...");
                        
                    } else {
                        $this->session->set_userdata('Error', "Please Try agin");
                        
                    }
                } else {
                    $this->session->set_userdata('Error', "Please Try Again");
                    
                }
            }

            $a_id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($a_id);

            $data['cat'] = $this->query->get_cat_detail($id);
            $data['catID'] = $id;
            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/edit_category', $data);
        } else {
            redirect('fyadmin/');
        }
    }

}
