<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
        $this->load->library('ssp');
    }
	
	public function get_view() {
		$total_segments = $this->uri->total_segments();
		$segments = $this->uri->segment($total_segments);
		return $segments;
	}
	public function index() {
        if ($this->session->userdata('Is_Login') == true) {
			$id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);		
			/* status */
			$id            	      =  $this->input->get('i');
			$ac            	      =  $this->input->get('ac');
			$status            	  =  $this->input->get('status');
			if($status == "yes"){
					
						if($ac==1){
							 $ac = 0;
							 $txt = 'Disabled';
							 }else{
							 $ac = 1;
							 $txt = 'Enabled';
						 }
					
						$data = array(
						   'status' => $ac
						);


						$table   = 'pages_post';
						$key	 = 'pageId';
						$query = $this->query->status($table, $data, $key, $id, $ac);
					
					
						if($query) {	
				
						$this->session->set_userdata('Success',"Record Has Been Successfully ".$txt.".");
						redirect(base_url().'fyadmin/pages');
						
						}
				
				}		
			/* end status */	
			/* delete */
			$id            	      =  $this->input->get('i');
			$delete            	  =  $this->input->get('delete');
			if($delete == "yes"){
				/* delete pages */
			$table = 'pages_post';
            $key   = 'pageId';
            $query = $this->query->delete($table, $key, $id);
			   /* delete pages details */
			$table = 'pages_post_images';
            $key   = 'pageId';
            $query = $this->query->delete($table, $key, $id);
				if($query) {	
                $this->session->set_userdata('Success', "Record Has Been Successfully Deleted...");
				redirect(base_url().'fyadmin/pages');
				}
			}	
			/* end delete */			
			$data['page_load'] = $this->get_view();
			$row = $this->query->query("SELECT * FROM `pages_post` WHERE `pages_post_type` = '1' ORDER BY `pageId` DESC");
			$data['pages'] = $row;
			$data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/pages', $data);
        } else {
            redirect('fyadmin/');
        }
    }
	
	public function add_edit_pages() {
        if ($this->session->userdata('Is_Login') == true) {
			$id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);			
			/* edit slider*/
			$id            	      =  $this->input->get('i');
			$delete_id            =  $this->input->get('id');
			$delete            	  =  $this->input->get('delete');
			$edit            	  =  $this->input->get('edit');
			if($edit == "yes"){
				$row = $this->query->query("SELECT * FROM `pages_post` WHERE `pageId` ='".base64_decode($id)."'");
				$data['pages'] = $row;
				
				$row = $this->query->query("SELECT * FROM `pages_post_images` WHERE `pageId` ='".$data['pages']['0']->pageId."'");
				$data['pages_details'] = $row;
				 
					/* delete slider images */
				if($delete == "image"){		
				$table = 'pages_post_images';
				$key   = 'imageId';
				$query = $this->query->delete($table, $key, $delete_id);
					if($query) {
	                $this->session->set_userdata('Success', "Image Has Been Successfully Deleted...");					
					redirect(base_url().'fyadmin/pages/add_edit_pages?i='.$id.'&edit=yes');				
					}
				}
				   /* end delete slider images */
				   
			}
			/* end edit slider*/
			$row = $this->query->query("SELECT * FROM `pages_post` WHERE `pages_post_type` = '1' ORDER BY `title` ASC");
			$data['underPage'] = $row;
			$data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/add_edit_pages', $data);
        } else {
            redirect('fyadmin/');
        }
    }
	public function do_add_pages() {
		
		$title       		= $this->input->post('title');
		$heading       		= $this->input->post('heading');
		$metaKeyword       	= $this->input->post('metaKeyword');
		$metaDescription    = $this->input->post('metaDescription');
		$Image       		= $_FILES['Image']['name'];
		$description       	= $this->input->post('description');
		$pages_post_type    = 1;
		$SEO       			= $this->custom->seo_Url($title);
		$created       		= date("Y-m-d H:i:s");
		$accountId       	= $this->session->userdata('AccountId');
		$orderBy       		= $this->input->post('orderBy');
		$underPage       	= $this->input->post('underPage');
		$ShowOnHeader       = $this->input->post('ShowOnHeader');
		$btn       			= $this->input->post('btn');
		
		
		$row = $this->query->query("SELECT * FROM `pages_post` WHERE `pages_post_type` = '1' AND `SEO` = '".$SEO."' ORDER BY `title` ASC");
		$data['underPage'] = $row;
		if($data['underPage']){
		 $SEO = $SEO.date('-M-d-Y-').$data['underPage'][0]->pageId;
		}else{
		 $SEO = $SEO;
		}
		
			$data = array(
				   'title'			=> $title,
				   'heading'		=> $heading,
				   'metaKeyword'	=> $metaKeyword,
				   'metaDescription'=> $metaDescription,
				   'description'	=> $description,
				   'pages_post_type'=> $pages_post_type,
				   'SEO'			=> $SEO,
				   'created'		=> $created,
				   'accountId'		=> $accountId,
				   'orderBy'		=> $orderBy,
				   'underPage'		=> $underPage,
				   'ShowOnHeader'	=> $ShowOnHeader,
				   'status'			=> 1,
			);
			
			$table     = 'pages_post';
			$query     = $this->query->insert_query($table, $data);
		    $CreatedID = $query['CreatedID'];
		if($query['query']==1) {
			
			$name_array = array();
				
			$count = count($_FILES['Image']['size']);
			foreach($_FILES as $key=>$value)
			for($s=0; $s<=$count-1; $s++) {
			$_FILES['Image']['name']	 = $value['name'][$s];
			$_FILES['Image']['type']     = $value['type'][$s];
			$_FILES['Image']['tmp_name'] = $value['tmp_name'][$s];
			$_FILES['Image']['error']    = $value['error'][$s];
			$_FILES['Image']['size']     = $value['size'][$s];  
			
			$config['upload_path']   = 'assets/upload/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$this->load->library('upload', $config);
			
			$this->upload->do_upload('Image');
			$data  = $this->upload->data();
			$Image = $data['file_name'];
			//$name_array[] = $data['file_name'];
				if($Image){	
					$data = array(
					   'Image'		=> $Image,
					   'pageId'		=> $CreatedID
					);
					$table     = 'pages_post_images';
					$query     = $this->query->insert_query($table, $data);
				}
			}
			$this->session->set_userdata('Success',"Record Has Been Successfully Added...");
			if($btn==1){
				redirect(base_url().'fyadmin/pages');
			}else{
				redirect(base_url().'fyadmin/pages/add_edit_pages');				
			}
		}else {
			$this->session->set_userdata('Error',"Please Try Again...");
			redirect(base_url().'fyadmin/');
		}
	}
	
	public function do_edit_pages() {
		
		$pageId       		= $this->input->post('pageId');
		$title       		= $this->input->post('title');
		$heading       		= $this->input->post('heading');
		$metaKeyword       	= $this->input->post('metaKeyword');
		$metaDescription    = $this->input->post('metaDescription');
		$Image       		= $_FILES['Image']['name'];
		$description       	= $this->input->post('description');
		$pages_post_type    = 1;
		$SEO       			= $this->custom->seo_Url($title);
		$created       		= date("Y-m-d H:i:s");
		$accountId       	= $this->session->userdata('AccountId');
		$orderBy       		= $this->input->post('orderBy');
		$underPage       	= $this->input->post('underPage');
		$ShowOnHeader       = $this->input->post('ShowOnHeader');
		$btn       			= $this->input->post('btn');
		
			$data = array(
				   'title'			=> $title,
				   'heading'		=> $heading,
				   'metaKeyword'	=> $metaKeyword,
				   'metaDescription'=> $metaDescription,
				   'description'	=> $description,
				   'pages_post_type'=> $pages_post_type,
				   'SEO'			=> $SEO,
				   'created'		=> $created,
				   'accountId'		=> $accountId,
				   'orderBy'		=> $orderBy,
				   'underPage'		=> $underPage,
				   'ShowOnHeader'	=> $ShowOnHeader,
				   'status'			=> 1,
			);
			
			$table   = 'pages_post';
            $key     = 'pageId';
            $query   = $this->query->update_query($table, $data, $key, $pageId);
			
			if($query){
					$name_array = array();
					
					$count = count($_FILES['Image']['size']);
					foreach($_FILES as $key=>$value)
					for($s=0; $s<=$count-1; $s++) {
					$_FILES['Image']['name']	 = $value['name'][$s];
					$_FILES['Image']['type']     = $value['type'][$s];
					$_FILES['Image']['tmp_name'] = $value['tmp_name'][$s];
					$_FILES['Image']['error']    = $value['error'][$s];
					$_FILES['Image']['size']     = $value['size'][$s];  
					
					$config['upload_path']   = 'assets/upload/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->load->library('upload', $config);
					
					$this->upload->do_upload('Image');
					$data  = $this->upload->data();
					$Image = $data['file_name'];
					//$name_array[] = $data['file_name'];
					if($Image){
						$data = array(
						   'Image'		=> $Image,
						   'pageId'		=> $pageId
						);
						$table     = 'pages_post_images';
						$query     = $this->query->insert_query($table, $data);
					}
				}
		    $this->session->set_userdata('Success', "Record Has Been Successfully Updated...");
			if($btn==1){
				redirect(base_url().'fyadmin/pages/');
			}else{
				redirect(base_url().'fyadmin/pages/add_edit_pages?i='.base64_encode($pageId).'&edit=yes');				
			}
		}else {
			$this->session->set_userdata('Error',"Please Try Again...");
			   redirect(base_url().'fyadmin/pages/add_edit_pages?i='.base64_encode($pageId).'&edit=yes');				
		}
	}
		
	public function pages_Json() {
        $row = $this->query->query("SELECT * FROM `pages_post` WHERE `pages_post_type` = '1' ORDER BY pageId DESC");
		$data['pages_Json'] = $row;
		$count=1;
		foreach($data['pages_Json'] as $pj){
			
		$status = '<a class="waves-effect waves-dark btn blue" title="Status" href="'.base_url().'fyadmin/pages?i='.base64_encode($pj->pageId).'&ac='.$pj->status.'&status=yes">';
		if($pj->status==1){
		$status .= '<i class="mdi-action-thumb-down"></i>';
		}else{
		$status .= '<i class="mdi-action-thumb-up"></i>';
		}
		$status .= '</a> ';
		$status .= '<a class="waves-effect waves-dark btn purple" title="Edit" href="'.base_url().'fyadmin/pages/add_edit_pages?i='.base64_encode($pj->pageId).'&edit=yes"><i class="mdi-editor-mode-edit"></i></a>
				    <!--<a class="waves-effect waves-dark btn red" title="Delete"  href="'.base_url().'fyadmin/pages?i='.base64_encode($pj->pageId).'&delete=yes" onClick="return confirmDelete();"><i class="mdi-action-delete"></i></a>-->';
			
		$query = $this->query->query("SELECT * FROM `pages_post` WHERE `pages_post_type` = '1' AND `pageId` = '".$pj->underPage."' ORDER BY pageId DESC");
		$data['pages_Parent_Json'] = $query;
		
		if($data['pages_Parent_Json']){
	    $underPage =  $data['pages_Parent_Json'][0]->title;
		}else{
		$underPage = "---";
		}
	
			
			$rows[0] = $count++;									
			$rows[1] = $pj->title;
			$rows[2] = $underPage;
			$rows[3] = $pj->orderBy;
			$rows[4] = $pj->created;	
			$rows[5] = $status;	
			$response['aaData'][] = $rows;
		}
		echo json_encode($response,true);
    }
	
	
}