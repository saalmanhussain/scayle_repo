<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounts extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
        
    }
    
    public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
    
    public function index() {
        if ($this->session->userdata('Is_Login') == true) {
			$id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $this->load->view('fyadmin/add_account', $data);
        } else {
            redirect('fyadmin/');
        }
    }
    
    public function add_account() {
        if ($this->session->userdata('Is_Login') == true) {
            
            $data["msg"]="";
            if (isset($_POST['add_account'])) {
                
                if ($_FILES['profile_image']['tmp_name']) {
                    $config['file_name'] = $_FILES['profile_image']['name'];
                    $config['upload_path'] = 'assets/upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('profile_image');
                    $Image1 = $this->upload->data();

                    $profile_image = $Image1['file_name'];
                } else {
                    $profile_image = '';
                }
                
                if($profile_image != '')
                {
                    $key = '';
                    $Password = $this->input->post('password');
                    $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $Password, MCRYPT_MODE_CBC, md5(md5($key))));
                    
                    $data = array(
                        'name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'password' => $PasswordEncrypted,
                        'account_type' => $this->input->post('account_type'),
                        'seo' => $this->custom->seo_Url($this->input->post('name')),
						'status' => 1,
                        'created' => date("Y-m-d"),
                        'accountId_created' => $this->session->userdata('AccountId')
                    );

                    $check_email = $this->query->check_email($this->input->post('email'));

                    if (count($check_email) == 0) {
                        $table = 'accounts';
                        $query = $this->query->insert_query($table, $data);

                        if ($query['query'] == 1) {

                            $data = array(
                                'country' => $this->input->post('country'),
                                'city' => $this->input->post('city'),
                                'state' => $this->input->post('state'),
                                'address' => $this->input->post('address'),
                                'phone' => $this->input->post('phone'),
                                'created' => date("Y-m-d"),
                                'status' => 1,
                                'accountId' => $query['CreatedID'],
                                'Twitter' => $this->input->post('twitter'),
                                'Facebook' => $this->input->post('fb'),
                                'LinkedIn' => $this->input->post('linkedin'),
                                'Googleplus' => $this->input->post('gplus'),
                                'YouTube' => $this->input->post('youtube'),
                                'Instagram' => $this->input->post('instagram'),
                                'Pinterest' => $this->input->post('pinterest'),
                                'SkypeID' => $this->input->post('skype'),
                                'profile_pic' => $profile_image
                            );

                            $table = 'account_details';
                            $query = $this->query->insert_query($table, $data);

                            if ($query['query'] == 1) {
                                
                                $logo = $this->custom->site_Info();
                                $txt2 = "";
                                $to2 = $this->input->post('email');
                                $subject2 = "CSG New User registration";
                                $txt2 .= "<div style='text-align: center;'><img src='" . PATH . "upload/".$logo['Logo']."' width='40%' />"
                                        . "<h1 style='font-weight: bold'>New Account Created</h1></div><div style='font-size: 18px;'>"
                                        . "Dear " . ucwords($this->input->post('name')) . ",<br/>"
                                        . "Your New CSG Ranked Account has been created.<br/><br/>"
                                        . "Email: " . $this->input->post('email') . "<br/>"
                                        . "Password: " . $this->input->post('password') . "<br/><br/>"
                                        . "Best Regards,<br/>"
                                        . "CSG Ranked Account Team <br/>";

                                $from2 = 'info@csg.com';
                                $headers = "From: CSG Ranked Account <$from2>" . "\r\n";
                                $headers .= "MIME-Version: 1.0\r\n";
                                $headers .= "Content-type: text/html\r\n";

                                mail($to2, $subject2, $txt2, $headers);

                                $this->session->set_userdata('Success',"Account Added Successfully");
                            }
                            else
                            {
                                $this->session->set_userdata('Error',"Please Try agin");
                            }

                        } else {
                            $this->session->set_userdata('Error',"Please Try agin");
                        }
                    } else {
                        $this->session->set_userdata('Error',"Username Already Exist");
                    }
                }
                else
                {
                    $this->session->set_userdata('Error',"Please Try agin");
					redirect('fyadmin/');
                }
                
            }
            
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            
            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();
            $data['account_type'] = $this->query->get_account_type();
            $this->load->view('fyadmin/add_account', $data);
        } else {
            redirect('fyadmin/');
        }
    }
    
    public function view_account() {
        if ($this->session->userdata('Is_Login') == true) {
            $data['msg'] = "";
			$id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['page_active'] = $this->get_view();
            $data['accounts'] = $this->query->get_all_account();
            $this->load->view('fyadmin/view_account',$data);
        } else {
            redirect('fyadmin/');
        }
    }
    
    public function view_account_Json() {
        $status = "";
        $accounts = $this->query->get_all_account();
		$count = 0;
        foreach ($accounts as $row) {
            
            $status .= "<a class='waves-effect waves-dark btn purple' title='Edit' href='".base_url()."fyadmin/accounts/edit_account?id=".base64_encode($row->accountId)."'>
                        <i class='mdi-editor-mode-edit'></i></a>
                        <a class='waves-effect waves-dark btn purple' title='Delete' href='".base_url()."fyadmin/accounts/complete_delete_account?id=".base64_encode($row->accountId)."'>
                        <i class='mdi-action-delete'></i></a>";
            if($row->status == 0)
            {
                $status .= "<a class='waves-effect waves-dark btn red' title='Enable' href='".base_url()."fyadmin/accounts/active_account?id=".base64_encode($row->accountId)."'>
                        <i class='mdi-action-thumb-up'></i></a>";
            }
            else
            {
              $status .= "<a class='waves-effect waves-dark btn red' title='Disable' href='".base_url()."fyadmin/accounts/delete_account?id=".base64_encode($row->accountId)."'>
                        <i class='mdi-action-thumb-down'></i></a>";  
            }
            $rows[0] = $count++;
            $rows[1] = $row->name;
            $rows[2] = $row->email;
            $rows[3] = $row->account_type;
            $rows[4] = $status;
            $response['aaData'][] = $rows;
            $status = "";
        }
        
        echo json_encode($response, true);
    }
    
    public function complete_delete_account()
    {
        if ($this->session->userdata('Is_Login') == true)
        {
            $id = ($_GET['id']);
        
            
            
            $table = 'accounts';
            $key   = 'accountId';
            $query = $this->query->delete($table, $key, $id);
            
            $table = 'account_details';
            $key   = 'accountId';
            $query = $this->query->delete($table, $key, $id);
            
            $table = 'customer_order';
            $key   = 'customerNo';
            $query = $this->query->delete($table, $key, $id);
            
            
            if($query)
            {
                $this->session->set_userdata('Success',"Account Deleted Successfully");
            }
            else
            {
                $this->session->set_userdata('Error',"Please Try Again");
            }
            
            redirect(base_url().'fyadmin/accounts/view_account');
            
            
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();
            $data['accounts'] = $this->query->get_all_account();
            $this->load->view('fyadmin/view_account',$data);
        }
        else {
            redirect('fyadmin/');
        }
    }
    
    public function delete_account()
    {
        if ($this->session->userdata('Is_Login') == true)
        {
            $id = base64_decode($_GET['id']);
        
            $data = array(
                'status' => 0
            );

            $query = $this->query->update_query('accounts',$data,'accountId',$id);
            
            if($query == 1)
            {
                $this->session->set_userdata('Success',"Account Disabled Successfully");
            }
            else
            {
                $this->session->set_userdata('Error',"Please Try Again");
            }
            
            redirect(base_url().'fyadmin/accounts/view_account');
            
            
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();
            $data['accounts'] = $this->query->get_all_account();
            $this->load->view('fyadmin/view_account',$data);
        }
        else {
            redirect('fyadmin/');
        }
    }
    
    public function active_account()
    {
        if ($this->session->userdata('Is_Login') == true)
        {
            $id = base64_decode($_GET['id']);
        
            $data = array(
                'status' => 1
            );

            $query = $this->query->update_query('accounts',$data,'accountId',$id);
            
            if($query == 1)
            {
                $this->session->set_userdata('Success',"Account Activated Successfully");
            }
            else
            {
                $this->session->set_userdata('Error',"Please Try Again");
            }
            
            redirect(base_url().'fyadmin/accounts/view_account');
            
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();
            $data['accounts'] = $this->query->get_all_account();
            $this->load->view('fyadmin/view_account',$data);
        }
        else {
            redirect('fyadmin/');
        }
    }
    
    public function edit_account() {
        
        $id = base64_decode($_GET['id']);
        
        if ($this->session->userdata('Is_Login') == true) {
            $data['msg'] = "";
            
            if (isset($_POST['add_account'])) {
                
                if ($_FILES['profile_image']['tmp_name']) {
                    $config['file_name'] = $_FILES['profile_image']['name'];
                    $config['upload_path'] = 'assets/upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('profile_image');
                    $Image1 = $this->upload->data();

                    $profile_image = $Image1['file_name'];
                } else {
                    $profile_image = $this->input->post('old_profile_image');
                }
                
                if($profile_image != '')
                {
                    $key = '';
                    $Password = $this->input->post('password');
                    $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $Password, MCRYPT_MODE_CBC, md5(md5($key))));
                    
                    $data = array(
                        'name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'password' => $PasswordEncrypted,
                        'account_type' => $this->input->post('account_type'),
						'seo' => $this->custom->seo_Url($this->input->post('name')),
                        'status' => 1,
                        'updated' => date("Y-m-d"),
                        'accountId_updated' => $this->session->userdata('AccountId')
                    );



                    $table = 'accounts';
                    $user_id = $this->input->post('accountID');

                    $query = $this->query->update_query('accounts', $data, 'accountId', $user_id);

                    $data = array(
                        'country' => $this->input->post('country'),
                        'city' => $this->input->post('city'),
                        'state' => $this->input->post('state'),
                        'address' => $this->input->post('address'),
                        'phone' => $this->input->post('phone'),
                        'updated' => date("Y-m-d"),
                        'status' => 1,
                        'Twitter' => $this->input->post('twitter'),
                        'Facebook' => $this->input->post('fb'),
                        'LinkedIn' => $this->input->post('linkedin'),
                        'Googleplus' => $this->input->post('gplus'),
                        'YouTube' => $this->input->post('youtube'),
                        'Instagram' => $this->input->post('instagram'),
                        'Pinterest' => $this->input->post('pinterest'),
                        'SkypeID' => $this->input->post('skype'),
                        'profile_pic' => $profile_image
                    );

                    $table = 'account_details';
                    $query = $this->query->update_query('account_details', $data, 'accountId', $user_id);

                    if ($query == 1) {
                        $this->session->set_userdata('Success',"Account Updated Successfully");
                        //$data['msg'] = "<p class='login-box-msg' style='color: green;'>Account Updated Successfully</p>";
                    } else {
                        $this->session->set_userdata('Error',"Please Try agin");
                        //$data['msg'] = "<p class='login-box-msg' style='color: red;'>Please Try agin</p>";
                    }
                }
                else
                {
                    $this->session->set_userdata('Error',"Please Try agin");
                }
            }
            
            $a_id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($a_id);
            
            $data['user'] = $this->query->get_user_detail($id);
            $data['accountId'] = $id;
            $data['account_type'] = $this->query->get_account_type();
            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/edit_account', $data);
        }
        else {
            redirect('fyadmin/');
        }
    }
    
    
    public function edit_profile() {
        
        if ($this->session->userdata('Is_Login') == true) {
            $data['msg'] = "";
            
            if (isset($_POST['add_account'])) {
                
                if ($_FILES['profile_image']['tmp_name']) {
                    $config['file_name'] = $_FILES['profile_image']['name'];
                    $config['upload_path'] = 'assets/upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('profile_image');
                    $Image1 = $this->upload->data();

                    $profile_image = $Image1['file_name'];
                } else {
                    $profile_image = $this->input->post('old_profile_image');
                }
                
                if($profile_image != '')
                {
                    $key = '';
                    $Password = $this->input->post('password');
                    $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $Password, MCRYPT_MODE_CBC, md5(md5($key))));
                    
                    
                    $data = array(
                        'name' => $this->input->post('name'),
                        'password' => $PasswordEncrypted,
                        'status' => 1,
                        'updated' => date("Y-m-d"),
                        'accountId_updated' => $this->session->userdata('AccountId')
                    );



                    $table = 'accounts';
                    $user_id = $this->input->post('accountID');

                    $query = $this->query->update_query('accounts', $data, 'accountId', $user_id);

                    $data = array(
                        'country' => $this->input->post('country'),
                        'city' => $this->input->post('city'),
                        'state' => $this->input->post('state'),
                        'address' => $this->input->post('address'),
                        'phone' => $this->input->post('phone'),
                        'updated' => date("Y-m-d"),
                        'status' => 1,
                        'Twitter' => $this->input->post('twitter'),
                        'Facebook' => $this->input->post('fb'),
                        'LinkedIn' => $this->input->post('linkedin'),
                        'Googleplus' => $this->input->post('gplus'),
                        'YouTube' => $this->input->post('youtube'),
                        'Instagram' => $this->input->post('instagram'),
                        'Pinterest' => $this->input->post('pinterest'),
                        'SkypeID' => $this->input->post('skype'),
                        'profile_pic' => $profile_image
                    );

                    $table = 'account_details';
                    $query = $this->query->update_query('account_details', $data, 'accountId', $user_id);

                    if ($query == 1) {
                        $this->session->set_userdata('Success',"Profile Updated Successfully");
                    } else {
                        $this->session->set_userdata('Error',"Please Try agin");
                    }
                }
                else
                {
                    $this->session->set_userdata('Error',"Please Try agin");
                }
            }
            
            $id = $this->session->userdata('AccountId');
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['user'] = $this->query->get_user_detail($id);
            $data['accountId'] = $id;
            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/edit_profile', $data);
        }
        else {
            redirect('fyadmin/');
        }
    }
    
    public function payment_setting() {
        if ($this->session->userdata('Is_Login') == true) {
            
            $data["msg"]="";
            if (isset($_POST['add_account'])) {
                
                if($this->session->userdata('Account_Type') == 1)
                {
                    if($this->input->post('paypal_id') != '' && $this->input->post('barclay_id') != '' && $this->input->post('skrill_id') != '')
                    {
                        $data = array(
                            'api_key' => $this->input->post('api_key'),
                            'email' => $this->input->post('email')
                        );
                        
                        $query = $this->query->update_query('payment_setting', $data, 'id', $this->input->post('paypal_id'));
                        
                        $data = array(
                            'api_key' => $this->input->post('api_key2'),
                            'email' => $this->input->post('email')
                        );
                        
                        $query = $this->query->update_query('payment_setting', $data, 'id', $this->input->post('barclay_id'));
                        
                        $data = array(
                            'api_key' => $this->input->post('api_key3'),
                            'email' => $this->input->post('email')
                        );
                        
                        $query = $this->query->update_query('payment_setting', $data, 'id', $this->input->post('skrill_id'));
                        
                        if ($query == 1) {
                            $this->session->set_userdata('Success',"Keys Updated Successfully");
                        } else {
                            $this->session->set_userdata('Error',"Please Try agin");
                        }
                    }
                    else 
                    {
                        $data = array(
                            'api_key' => $this->input->post('api_key'),
                            'email' => $this->input->post('email'),
                            'type' => 'paypal',
                            'created' => date("Y-m-d"),
                            'accountId_created' => $this->session->userdata('AccountId')
                        );

                        $table = 'payment_setting';
                        $query = $this->query->insert_query($table, $data);

                        $data = array(
                            'api_key' => $this->input->post('api_key2'),
                            'email' => $this->input->post('email'),
                            'type' => 'barclay',
                            'created' => date("Y-m-d"),
                            'accountId_created' => $this->session->userdata('AccountId')
                        );

                        $table = 'payment_setting';
                        $query = $this->query->insert_query($table, $data);

                        $data = array(
                            'api_key' => $this->input->post('api_key3'),
                            'email' => $this->input->post('email'),
                            'type' => 'skrill',
                            'created' => date("Y-m-d"),
                            'accountId_created' => $this->session->userdata('AccountId')
                        );

                        $table = 'payment_setting';
                        $query = $this->query->insert_query($table, $data);

                        if ($query['query'] == 1) {
                            $this->session->set_userdata('Success',"Keys Updated Successfully");
                        }
                        else
                        {
                            $this->session->set_userdata('Error', "Please Try agin");
                        }
                    }
                    
                }
                else if($this->session->userdata('Account_Type') != 1)
                {
                    if($this->input->post('paypal_id') != '')
                    {
                        $data = array(
                            'api_key' => $this->input->post('api_key'),
                            'email' => $this->input->post('email')
                        );
                        
                        $query = $this->query->update_query('payment_setting', $data, 'id', $this->input->post('paypal_id'));
                        
                        if ($query == 1) {
                            $this->session->set_userdata('Success',"Keys Updated Successfully");
                        } else {
                            $this->session->set_userdata('Error',"Please Try agin");
                        }
                    }
                    else {
                        $data = array(
                            'api_key' => $this->input->post('api_key'),
                            'email' => $this->input->post('email'),
                            'type' => 'paypal',
                            'created' => date("Y-m-d"),
                            'accountId_created' => $this->session->userdata('AccountId')
                        );

                        $table = 'payment_setting';
                        $query = $this->query->insert_query($table, $data);

                        if ($query['query'] == 1) {
                            $this->session->set_userdata('Success',"Keys Updated Successfully");
                        }
                        else
                        {
                            $this->session->set_userdata('Error', "Please Try agin");
                        }
                    }
                }
                else
                {
                    $this->session->set_userdata('Error',"Please Try agin");
                }
                
            }
            
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            
            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();
            $data['payment'] = $this->query->get_account_payment_setting($id);
            $this->load->view('fyadmin/payment_setting', $data);
            
        } else {
            redirect('fyadmin/');
        }
    }

    public function orders()
    {
        if ($this->session->userdata('Is_Login') == true) {
            $data['msg'] = "";

            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);

            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();

            $data['order'] = $this->query->get_orders();
			
			if($this->input->get('id')){
					echo $oid = $_GET['id'];
					echo $table = 'customer_order';
					echo  $key   = 'orderId';
					$query = $this->query->delete($table, $key, $oid);
					
					$this->session->set_userdata('Success',"Order Deleted Successfully.");
					redirect('fyadmin/accounts/orders');
            
			}
			if($this->input->get('send_email') != null && $this->input->get('orderId')){
				$send_email = $this->input->get('send_email');
				$orderId    = $this->input->get('orderId');
				$data = array(
					'send_email' => $send_email
				);
				$query = $this->query->update_query('customer_order',$data,'orderId',$orderId);
				$this->session->set_userdata('Success',"Send Email Action has been changed.");
				redirect('fyadmin/accounts/orders');
			}	
			
			if($this->input->get('order_status') != null && $this->input->get('orderId')){
				$status     = $this->input->get('order_status');
				$orderId    = $this->input->get('orderId');
				
				$data = array(
					'status' => $status
				);
				
				if($status==1){
					    $orderNo    = $this->input->get('orderNo');
						$products   = $this->query->get_order_details_orderno($orderNo);
				
						$product_id = explode(",", $products[0]->item_id);
						
						for($i = 0; $i < count($product_id) ; $i++)
						{
							$array = array(
								'status' => 2 //sold
							);
							
							$query = $this->query->update_query("product", $array, 'proID', $product_id[$i]);
						}
				}
				$query = $this->query->update_query('customer_order',$data,'orderId',$orderId);
				$this->session->set_userdata('Success',"Order Status Action has been changed.");
				redirect('fyadmin/accounts/orders');
			}
			
            $this->load->view('fyadmin/orders', $data);
        }
        else
        {
           redirect('fyadmin/'); 
        }
    }
    
    public function order_details()
    {
        if ($this->session->userdata('Is_Login') == true) {
            $o_id = base64_decode($_GET['id']);
			
				$data = array(
					'view_order' => 2
				);
				$query = $this->query->update_query('customer_order',$data,'orderId',$o_id);
			
            $data['msg'] = "";

            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);

            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();

            $data['order_detail'] = $this->query->get_order_details($o_id);

            $this->load->view('fyadmin/order_detail', $data);
        }
        else
        {
           redirect('fyadmin/'); 
        }
    }
    
    public function send_email()
    {
        if ($this->session->userdata('Is_Login') == true) {
            $txt2 = "";
            $site_Info = $this->custom->site_Info();
            $subject2 = "Order Email";
            $txt2 .= $this->input->post('email_text');
			
            $from2 = $site_Info['EmailFrom'];
            $cc = $site_Info['EmailTo'];
            $headers = "From: CSG Ranked Account <$from2>" . "\r\n";
            $headers .= "Cc: <$cc>". "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html\r\n";

            mail($this->input->post('email'), $subject2, $txt2, $headers);

            $this->session->set_userdata('Success',"Email Send Successfully");
				
				$orderId = $this->input->post('orderId');
				
				$data = array(
					'send_email' => 1
				);
				$query = $this->query->update_query('customer_order',$data,'orderId',$orderId);
				redirect('fyadmin/accounts/orders');
        }
        else
        {
           redirect('fyadmin/'); 
        }
    }
}
