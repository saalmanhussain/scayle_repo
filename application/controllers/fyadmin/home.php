<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('Custom');
        
    }

    public function index() {

        /* -----login----- */

        $data['site_Info'] = $this->custom->site_Info();

        $data['error'] = "";

        if (isset($_POST['login-btn'])) {
            $data['error'] = "";
            $Email = $this->input->post('username');
            $Password = $this->input->post('password');
            $key = ''; //csg
            
            $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $Password, MCRYPT_MODE_CBC, md5(md5($key))));
            
            //$PasswordEncrypted = md5($Password);
            
            $this->load->model('fyadmin/query');
            
            $query = $this->query->login($Email, $PasswordEncrypted);
            
            if ($query->num_rows() == 1) {
                foreach ($query->result_array() as $recs => $res) {
                    $this->session->set_userdata(array(
                        'AccountId' 	=> $res['accountId'],
                        'name' 			=> $res['name'],
                        'Account_Type' 	=> $res['account_type'],
                        'Is_Login' 		=> true
                            )
                    );
                }

                redirect(base_url() . 'fyadmin/dashboard');
            } else {
                $data['error'] = "Invalid Email Address/Password.";
                $this->load->view('fyadmin/home', $data);
            }
        } else {
            
            $this->load->view('fyadmin/home', $data);
        }
        /* ----- //login----- */


        if ($this->session->userdata('Is_Login') == true) {
            redirect(base_url() . 'fyadmin/dashboard');
        }
    }

    /* -----Logout----- */

    public function logout() {
        $this->session->unset_userdata('AccountId');
        $this->session->unset_userdata('Account_Type');
        $this->session->unset_userdata('Is_Login');
        $this->session->sess_destroy();

        redirect(base_url() . 'fyadmin');
    }

    /* -----//Logout----- */

    public function forgot_pasword() {
        
        $this->load->model('fyadmin/query');

        $data['error'] = "";

        if (isset($_POST['forgot-btn'])) {
            $data['error'] = "";

            $email = $this->input->post('email');

            $passwordlength = 7;
            $new_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $passwordlength);
            $key = '';
            
            $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $new_pass, MCRYPT_MODE_CBC, md5(md5($key))));
                    

            $reset = array('password' => $PasswordEncrypted);

            $table = 'accounts';

            $query = $this->query->do_edit($table, $reset, $email);


            $txt2 = "";
            $to2 = $email;
            $subject2 = "Reset Password";
            $txt2 .= "Password successfully Reset\n\n New Password:  " . $new_pass . "\n\nRegards,\nCSG Ranked Accounts.";
            $from2 = 'info@csg.com';
            $headers = "From: CSG Ranked Account" . "\r\n";

            mail($to2, $subject2, $txt2, $headers);

            $this->load->library('email');

            $config['protocol'] = 'sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';

            $this->email->initialize($config);

            $this->email->set_newline("\r\n");
            $this->email->from('info@csg.com', 'CSG Ranked Accounts');
            $this->email->to($email);

            //$this->email->subject('Forget Password');

            $this->email->subject("Reset Password");
            $this->email->message('Password successfully Reset\n\nNew Password: ' . $new_pass);

            $data['error'] = "Password update please check email";
        }
        $data['site_Info'] = $this->custom->site_Info();
        $this->load->view('fyadmin/forgot_password', $data);
    }

}
