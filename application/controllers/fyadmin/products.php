<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }

    public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }

    public function index() {
        
    }

    public function add_product() {
        if ($this->session->userdata('Is_Login') == true) {

            $data["msg"] = "";

            if (isset($_POST['add_product'])) {

                if ($_FILES['pro_image']['tmp_name']) {
                    $config['file_name'] = $_FILES['pro_image']['name'];
                    $config['upload_path'] = 'assets/upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('pro_image');
                    $Image1 = $this->upload->data();

                    $pro_image = $Image1['file_name'];
                } else {
                    $pro_image = '';
                }

                

                    $data = array(
                        'title' 			=> $this->input->post('title'),
                        'SubText' 			=> $this->input->post('SubText'),
                        'Price' 			=> $this->input->post('Price'),
                        'OldPrice' 			=> $this->input->post('OldPrice'),
                        'seo' 				=> $this->custom->seo_Url($this->input->post('title')),
                        'description'   	=> $this->input->post('description'),
                        'image' 			=> $pro_image,
                        'cat_id' 			=> $this->input->post('category'),
                        'status'	    	=> 1,
                        'created' 			=> date("Y-m-d"),
                        'accountId_created' => $this->input->post('AccountId')
                    );



                    $table = 'product';
                    $query = $this->query->insert_query($table, $data);
				 if ($query['query'] == 1) {
					$this->session->set_userdata('Success', "Record Has Been Successfully Added...");
					redirect('fyadmin/products/add_product');
                } else {
                    $this->session->set_userdata('Error', "Please Try Again");
                    redirect('fyadmin/products/add_product');
                }
            }

            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['page_active'] = $this->get_view();
            $data['cat'] = $this->query->get_all_active_categories();
			$data['partner'] = $this->query->get_all_partner();
            $this->load->view('fyadmin/add_product', $data);
        } else {
            redirect('fyadmin/');
        }
    }
 
 
	public function view_product_sold() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['msg'] = "";
            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/view_product_sold', $data);
        } else {
            redirect('fyadmin/');
        }
    }
	public function view_product_Json_sold() {
        $status = "";
        
        if($this->session->userdata('Account_Type') == 1)
        {
            $product = $this->query->get_all_products_sold();
        }
        else
        {
            $product = $this->query->get_all_products_users_sold($this->session->userdata('AccountId'));
        }
        $count=1;
        foreach ($product as $row) {

            $status .= "<a class='waves-effect waves-dark btn purple' title='Edit' href='" . base_url() . "fyadmin/products/edit_product?id=" . base64_encode($row->proID) . "'>
                        <i class='mdi-editor-mode-edit'></i></a>";

            if ($row->status == 0) {
                $status .= "<a class='waves-effect waves-dark btn red' title='Enable' href='" . base_url() . "fyadmin/products/activate_product?id=" . base64_encode($row->proID) . "'>
                        <i class='mdi-action-thumb-up'></i></a>";
            } else {
                $status .= "<a class='waves-effect waves-dark btn red' title='Disable' href='" . base_url() . "fyadmin/products/delete_product?id=" . base64_encode($row->proID) . "'>
                        <i class='mdi-action-thumb-down'></i></a>";
            }
            
            $rows[0] = $count++;
            $rows[1] = $row->title;
            $rows[2] = $row->username;
            $rows[3] = $row->product_url;
            $rows[4] = $row->cat_name;
            $rows[5] = "<img src='" . PATH . "upload/$row->image' height='100' />";
            $rows[6] = $row->dealer;
            $rows[7] = "$".$row->price;
            $rows[8] = date('Y-M-d',  strtotime($row->created));
            $response['aaData'][] = $rows;
            $status = "";
        }
        
        if(count($product) == 0)
        {
            $rows[0] = "";
            $rows[1] = "";
            $rows[2] = "";
            $rows[3] = "";
			$rows[4] = "";
            $rows[5] = "";
            $rows[6] = "";
            $rows[7] = "";
            $rows[8] = "";
            $response['aaData'][] = $rows;
        }

        echo json_encode($response, true);
    }
	
    public function view_product() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['msg'] = "";
            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/view_product', $data);
        } else {
            redirect('fyadmin/');
        }
    }
	

    public function view_product_Json() {
        $status = "";
        
        if($this->session->userdata('Account_Type') == 1)
        {
            $product = $this->query->get_all_products();
        }
        else
        {
            $product = $this->query->get_all_products_users($this->session->userdata('AccountId'));
        }
        $count=1;
        foreach ($product as $row) {

            $status .= "<a class='waves-effect waves-dark btn purple' title='Edit' href='" . base_url() . "fyadmin/products/edit_product?id=" . base64_encode($row->proID) . "'>
                        <i class='mdi-editor-mode-edit'></i></a>";

            if ($row->status == 0) {
                $status .= "<a class='waves-effect waves-dark btn red' title='Enable' href='" . base_url() . "fyadmin/products/activate_product?id=" . base64_encode($row->proID) . "'>
                        <i class='mdi-action-thumb-up'></i></a>";
            } else {
                $status .= "<a class='waves-effect waves-dark btn red' title='Disable' href='" . base_url() . "fyadmin/products/delete_product?id=" . base64_encode($row->proID) . "'>
                        <i class='mdi-action-thumb-down'></i></a>";
            }
            
            $rows[0] = $count++;
            $rows[1] = $row->title;
            $rows[2] = $row->SubText;
            $rows[3] = $row->cat_name;
            $rows[4] = $row->dealer;
            $rows[5] = date('Y-M-d',  strtotime($row->created));
            $rows[6] = $status;
            $response['aaData'][] = $rows;
            $status = "";
        }
        
        if(count($product) == 0)
        {
            $rows[0] = "";
            $rows[1] = "";
            $rows[2] = "";
            $rows[3] = "";
			$rows[4] = "";
            $rows[5] = "";
            $rows[6] = "";
            $response['aaData'][] = $rows;
        }

        echo json_encode($response, true);
    }

    public function delete_product() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = base64_decode($_GET['id']);

            $data = array(
                'status' => 0
            );

            $query = $this->query->update_query('product', $data, 'proID', $id);

            if ($query == 1) {
                $this->session->set_userdata('Success', "Product Disabled Successfully");
            } else {
                $this->session->set_userdata('Error', "Please Try Again");
            }
            
            redirect(base_url().'fyadmin/products/view_product');
            
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');

            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $this->load->view('fyadmin/view_product', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function activate_product() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = base64_decode($_GET['id']);

            $data = array(
                'status' => 1
            );

            $query = $this->query->update_query('product', $data, 'proID', $id);

            if ($query == 1) {
                $this->session->set_userdata('Success', "Product Enabled Successfully");
            } else {
                $this->session->set_userdata('Error', "Please Try Again");
            }
            
            redirect(base_url().'fyadmin/products/view_product');
            
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');

            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $this->load->view('fyadmin/view_product', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function edit_product() {

        if ($this->session->userdata('Is_Login') == true) {

            $id = base64_decode($_GET['id']);

            $data['msg'] = "";

            if (isset($_POST['edit_product'])) {

                if ($_FILES['pro_image']['tmp_name']) {
                    $config['file_name'] = $_FILES['pro_image']['name'];
                    $config['upload_path'] = 'assets/upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('pro_image');
                    $Image1 = $this->upload->data();

                    $pro_image = $Image1['file_name'];
                } else {
                    $pro_image = $this->input->post('pro_old_image');
                }


                    $data = array(
                        'title' 			=> $this->input->post('title'),
                        'SubText' 			=> $this->input->post('SubText'),
                        'Price' 			=> $this->input->post('Price'),
                        'OldPrice' 			=> $this->input->post('OldPrice'),
                        'seo' 				=> $this->custom->seo_Url($this->input->post('title')),
                        'description' 		=> $this->input->post('description'),
                        'image' 			=> $pro_image,
                        'cat_id' 			=> $this->input->post('category'),
                        'status' 			=> 1,
                        'updated' 			=> date("Y-m-d"),
                        'accountId_created' => $this->input->post('AccountId'),
                        'accountId_updated' => $this->input->post('AccountId')
                    );

                    $table = 'product';
                    $pro_id = $this->input->post('proID');

                    $query = $this->query->update_query($table, $data, 'proID', $pro_id);

                 if ($query == 1){
                        $this->session->set_userdata('Success', "Record Has Been Successfully Updated...");
                        redirect('fyadmin/products/edit_product?id='.base64_encode($pro_id));
                } else {
                    $this->session->set_userdata('Error', "Please Try Again");
                    redirect('fyadmin/products/edit_product?id='.base64_encode($pro_id));
                }
            }

            $a_id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($a_id);

            $data['cat'] = $this->query->get_all_active_categories();
            $data['pro'] = $this->query->get_pro_detail($id);
            $data['proID'] = $id;
            $data['page_active'] = $this->get_view();
			$data['partner'] = $this->query->get_all_partner();
            $this->load->view('fyadmin/edit_product', $data);
        } else {
            redirect('fyadmin/');
        }
    }
    
    public function delete_images()
    {
        $id = $_GET['id'];
        $img_id = $_GET['img_id'];
        
        $query = $this->query->delete('product_images','image_id',base64_encode($img_id));
        redirect(base_url().'fyadmin/products/edit_product?id='. base64_encode($id));
    }

}
