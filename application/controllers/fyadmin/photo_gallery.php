<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Photo_gallery extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
        $this->load->library('ssp');
    }
	
	public function get_view() {
		$total_segments = $this->uri->total_segments();
		$segments = $this->uri->segment($total_segments);
		return $segments;
	}
	public function index() {
        if ($this->session->userdata('Is_Login') == true) {
			$id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);		
			/* status */
			$id            	      =  $this->input->get('i');
			$ac            	      =  $this->input->get('ac');
			$status            	  =  $this->input->get('status');
			if($status == "yes"){
					
						if($ac==1){
							 $ac = 0;
							 $txt = 'Disabled';
							 }else{
							 $ac = 1;
							 $txt = 'Enabled';
						 }
					
						$data = array(
						   'status' => $ac
						);


						$table   = 'photo_gallery';
						$key	 = 'photo_gallery_Id';
						$query = $this->query->status($table, $data, $key, $id, $ac);
					
					
						if($query) {	
				
						$this->session->set_userdata('Success',"Record Has Been Successfully ".$txt.".");
						redirect(base_url().'fyadmin/photo_gallery');
						
						}
				
				}		
			/* end status */	
			/* delete */
			$id            	      =  $this->input->get('i');
			$delete            	  =  $this->input->get('delete');
			if($delete == "yes"){
				/* delete photo gallery */
			$table = 'photo_gallery';
            $key   = 'photo_gallery_Id';
            $query = $this->query->delete($table, $key, $id);
			   /* delete photo gallery Image */
			$table = 'photo_gallery_images';
            $key   = 'photo_gallery_Id';
            $query = $this->query->delete($table, $key, $id);
				if($query) {	
                $this->session->set_userdata('Success', "Record Has Been Successfully Deleted...");
				redirect(base_url().'fyadmin/photo_gallery');
				}
			}	
			/* end delete */			
			$data['page_load'] = $this->get_view();
			$row = $this->query->query("SELECT * FROM `photo_gallery` ORDER BY `photo_gallery_Id` DESC");
			$data['photo_gallery'] = $row;
			$data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/photo_gallery', $data);
        } else {
            redirect('fyadmin/');
        }
    }
	
	public function add_edit_photo_gallery() {
        if ($this->session->userdata('Is_Login') == true) {
			$id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);			
			/* edit slider*/
			$id            	      =  $this->input->get('i');
			$delete_id            =  $this->input->get('id');
			$delete            	  =  $this->input->get('delete');
			$edit            	  =  $this->input->get('edit');
			if($edit == "yes"){
				$row = $this->query->query("SELECT * FROM `photo_gallery` WHERE `photo_gallery_Id` ='".base64_decode($id)."'");
				$data['photo_gallery'] = $row;
				
				$row = $this->query->query("SELECT * FROM `photo_gallery_images` WHERE `photo_gallery_Id` ='".$data['photo_gallery']['0']->photo_gallery_Id."'");
				$data['gallery_details'] = $row;
				 
					/* delete slider images */
				if($delete == "image"){		
				$table = 'photo_gallery_images';
				$key   = 'gallery_images_Id';
				$query = $this->query->delete($table, $key, $delete_id);
					if($query) {
	                $this->session->set_userdata('Success', "Image Has Been Successfully Deleted...");					
					redirect(base_url().'fyadmin/photo_gallery/add_edit_photo_gallery?i='.$id.'&edit=yes');				
					}
				}
				   /* end delete slider images */
				   
			}
			/* end edit slider*/
			$data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/add_edit_photo_gallery', $data);
        } else {
            redirect('fyadmin/');
        }
    }
	public function do_add_photo_gallery() {
		
		$title       		= $this->input->post('title');
		$Image       		= $_FILES['Image']['name'];
		$description       	= $this->input->post('description');
		$pages_post_type    = 1;
		$SEO       			= $this->custom->seo_Url($title);
		$created       		= date("Y-m-d H:i:s");
		$accountId       	= $this->session->userdata('AccountId');
		$btn       			= $this->input->post('btn');
		
		
		$row = $this->query->query("SELECT * FROM `photo_gallery` WHERE `SEO` = '".$SEO."' ORDER BY `title` ASC");
		$data['photo_gallery'] = $row;
		if($data['photo_gallery']){
		 $SEO = $SEO.date('-M-d-Y-').$data['photo_gallery'][0]->photo_gallery_Id;
		}else{
		 $SEO = $SEO;
		}
		
			$data = array(
				   'title'			=> $title,
				   'description'	=> $description,
				   'SEO'			=> $SEO,
				   'created'		=> $created,
				   'accountId'		=> $accountId,
				   'status'			=> 1,
			);
			
			$table     = 'photo_gallery';
			$query     = $this->query->insert_query($table, $data);
		    $CreatedID = $query['CreatedID'];
		if($query['query']==1) {
				$name_array = array();
				
				$count = count($_FILES['Image']['size']);
				foreach($_FILES as $key=>$value)
				for($s=0; $s<=$count-1; $s++) {
				$_FILES['Image']['name']	 = $value['name'][$s];
				$_FILES['Image']['type']     = $value['type'][$s];
				$_FILES['Image']['tmp_name'] = $value['tmp_name'][$s];
				$_FILES['Image']['error']    = $value['error'][$s];
				$_FILES['Image']['size']     = $value['size'][$s];  
				
				$config['upload_path']   = 'assets/upload/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				
				$this->upload->do_upload('Image');
				$data  = $this->upload->data();
				$Image = $data['file_name'];
				//$name_array[] = $data['file_name'];
				
					$data = array(
					   'Image'				=> $Image,
					   'photo_gallery_Id'	=> $CreatedID
					);
					$table    		= 'photo_gallery_images';
					$query     		= $this->query->insert_query($table, $data);
				}
			
			$this->session->set_userdata('Success',"Record Has Been Successfully Added...");
			if($btn==1){
				redirect(base_url().'fyadmin/photo_gallery');
			}else{
				redirect(base_url().'fyadmin/photo_gallery/add_edit_photo_gallery');				
			}
		}else {
			$this->session->set_userdata('Error',"Please Try Again...");
			redirect(base_url().'fyadmin/');
		}
	}
	
	public function do_edit_photo_gallery() {
		
		$photo_gallery_Id   = $this->input->post('photo_gallery_Id');
		$title       		= $this->input->post('title');
		$Image       		= $_FILES['Image']['name'];
		$description       	= $this->input->post('description');
		$pages_post_type    = 1;
		$SEO       			= $this->custom->seo_Url($title);
		$created       		= date("Y-m-d H:i:s");
		$accountId       	= $this->session->userdata('AccountId');
		$btn       			= $this->input->post('btn');
		
			$data = array(
				   'title'			=> $title,
				   'description'	=> $description,
				   'SEO'			=> $SEO,
				   'created'		=> $created,
				   'accountId'		=> $accountId,
				   'status'			=> 1,
			);
			
			$table   = 'photo_gallery';
            $key     = 'photo_gallery_Id';
            $query   = $this->query->update_query($table, $data, $key, $photo_gallery_Id);
			
			if($query){
					$name_array = array();
					
					$count = count($_FILES['Image']['size']);
					foreach($_FILES as $key=>$value)
					for($s=0; $s<=$count-1; $s++) {
					$_FILES['Image']['name']	 = $value['name'][$s];
					$_FILES['Image']['type']     = $value['type'][$s];
					$_FILES['Image']['tmp_name'] = $value['tmp_name'][$s];
					$_FILES['Image']['error']    = $value['error'][$s];
					$_FILES['Image']['size']     = $value['size'][$s];  
					
					$config['upload_path']   = 'assets/upload/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->load->library('upload', $config);
					
					$this->upload->do_upload('Image');
					$data  = $this->upload->data();
					$Image = $data['file_name'];
					//$name_array[] = $data['file_name'];
					
						$data = array(
						   'Image'				=> $Image,
						   'photo_gallery_Id'	=> $photo_gallery_Id
						);
						$table     = 'photo_gallery_images';
						$query     = $this->query->insert_query($table, $data);
					}
		    $this->session->set_userdata('Success', "Record Has Been Successfully Updated...");
			if($btn==1){
				redirect(base_url().'fyadmin/photo_gallery/');
			}else{
				redirect(base_url().'fyadmin/photo_gallery/add_edit_photo_gallery?i='.base64_encode($photo_gallery_Id).'&edit=yes');				
			}
		}else {
			$this->session->set_userdata('Error',"Please Try Again...");
			   redirect(base_url().'fyadmin/photo_gallery/add_edit_photo_gallery?i='.base64_encode($photo_gallery_Id).'&edit=yes');				
		}
	}
		
	public function photo_gallery_Json() {
        $row = $this->query->query("SELECT * FROM `photo_gallery` ORDER BY photo_gallery_Id DESC");
		$data['pages_Json'] = $row;
		$count=1;
		foreach($data['pages_Json'] as $pj){
			
		$status = '<a class="waves-effect waves-dark btn blue" title="Status" href="'.base_url().'fyadmin/photo_gallery?i='.base64_encode($pj->photo_gallery_Id).'&ac='.$pj->status.'&status=yes">';
		if($pj->status==1){
		$status .= '<i class="mdi-action-thumb-down"></i>';
		}else{
		$status .= '<i class="mdi-action-thumb-up"></i>';
		}
		$status .= '</a> ';
		$status .= '<a class="waves-effect waves-dark btn purple" title="Edit" href="'.base_url().'fyadmin/photo_gallery/add_edit_photo_gallery?i='.base64_encode($pj->photo_gallery_Id).'&edit=yes"><i class="mdi-editor-mode-edit"></i></a>
				    <a class="waves-effect waves-dark btn red" title="Delete"  href="'.base_url().'fyadmin/photo_gallery?i='.base64_encode($pj->photo_gallery_Id).'&delete=yes" onClick="return confirmDelete();"><i class="mdi-action-delete"></i></a>';
			
		
			
			$rows[0] = $count++;									
			$rows[1] = $pj->title;
			$rows[2] = $pj->created;	
			$rows[3] = $status;	
			$response['aaData'][] = $rows;
		}
		echo json_encode($response,true);
    }
	
	
}