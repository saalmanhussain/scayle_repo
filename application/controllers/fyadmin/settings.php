<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
	public function index() {
        if ($this->session->userdata('Is_Login') == true) {
			$id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			/* settings*/
			$row = $this->query->query("SELECT * FROM `settings` WHERE `settingsId` ='1'");
			$data['settings'] = $row;
            /* end settings*/
             $data['page_active'] = "";
			 $this->load->view('fyadmin/settings', $data);
			} else {
            redirect('fyadmin/');
        }
    }
	public function do_edit_settings() {
		
		$title       		= $this->input->post('title');
		$metaKeyword       	= $this->input->post('metaKeyword');
		$metaDescription   	= $this->input->post('metaDescription');
		$emailTo       		= $this->input->post('emailTo');
		$emailFrom       	= $this->input->post('emailFrom');
		$mobile       		= $this->input->post('mobile');
		$phone       		= $this->input->post('phone');
		$address       		= $this->input->post('address');
		$logo       		= $_FILES['logo']['name'];
		$headerText       	= $this->input->post('headerText');
		$footerText       	= $this->input->post('footerText');
		$copyright       	= $this->input->post('copyright');
		$text_1       		= $this->input->post('text_1');
		$text_2       		= $this->input->post('text_2');
		$text_3       		= $this->input->post('text_3');

		
			if ($logo){
				$file_name 				 = 'img-' . $_FILES['logo']['name'];
				$config['upload_path']   = 'assets/upload/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';

				$config['file_name'] = $file_name;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$this->upload->do_upload('logo');
				$data = $this->upload->data();
				$logo = $data['file_name'];
				
				$data = array(
				'logo'				=> $logo
				);	
				$table      = 'settings';
				$key        = 'settingsId';
				$settingsId = 1;
				$query 	    = $this->query->update_query($table, $data, $key, $settingsId);
			
  			}
			$data = array(
				   'title'				=> $title,
				   'metaKeyword'		=> $metaKeyword,
				   'metaDescription'	=> $metaDescription,
				   'emailTo'			=> $emailTo,
				   'emailFrom'			=> $emailFrom,
				   'mobile'				=> $mobile,
				   'phone'				=> $phone,
				   'address'			=> $address,
				   'headerText'			=> $headerText,
				   'footerText'			=> $footerText,
				   'copyright'			=> $copyright,
				   'text_1'				=> $text_1,
				   'text_2'				=> $text_2,
				   'text_3'				=> $text_3
			);
			
			$table      = 'settings';
			$key        = 'settingsId';
			$settingsId = 1;
			$query 	    = $this->query->update_query($table, $data, $key, $settingsId);	
		
		if($query) {
			$this->session->set_userdata('Success',"Record Has Been Successfully Updated...");
			redirect(base_url().'fyadmin/settings');				
		}else {
			$this->session->set_userdata('Error',"Please Try Again...");
			redirect(base_url().'fyadmin/');
		}
	}
}