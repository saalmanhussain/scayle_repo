<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }

    public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }

    public function index() {
        if ($this->session->userdata('Is_Login') == true) {
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('Account_Type');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['page_active']  = $this->get_view();
            $this->load->view('fyadmin/dashboard', $data);
        } else {
            redirect('fyadmin/');
        }
    }
	 public function orders(){
        if ($this->session->userdata('Is_Login') == true) {
            $data['msg'] = "";

            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['site_Info'] = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);

            $data['site_Info'] = $this->custom->site_Info();
            $data['page_active'] = $this->get_view();

            $data['order'] = $this->query->get_orders();

            $this->load->view('fyadmin/orders_dashboard', $data);
        }
        else
        {
           redirect('fyadmin/'); 
        }
    }
}
