<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Slider extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
        $this->load->library('ssp');
    }

    public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }

    public function index() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['site_Info'] = $this->custom->site_Info();
            /* status */
            $id = $this->input->get('i');
            $ac = $this->input->get('ac');
            $status = $this->input->get('status');
            if ($status == "yes") {

                if ($ac == 1) {
                    $ac = 0;
                    $txt = 'Disabled';
                } else {
                    $ac = 1;
                    $txt = 'Enabled';
                }

                $data = array(
                    'status' => $ac
                );


                $table = 'slider';
                $key = 'sliderId';
                $query = $this->query->status($table, $data, $key, $id, $ac);


                if ($query) {

                    $this->session->set_userdata('Success', "Record Has Been Successfully " . $txt . ".");
                    redirect(base_url() . 'fyadmin/slider');
                }
            }
            /* end status */
            /* delete */
            $id = $this->input->get('i');
            $delete = $this->input->get('delete');
            if ($delete == "yes") {
                /* delete slider */
                $table = 'slider';
                $key = 'sliderId';
                $query = $this->query->delete($table, $key, $id);
                /* delete slider details */
                $table = 'slider_details';
                $key = 'sliderId';
                $query = $this->query->delete($table, $key, $id);
                if ($query) {
                    $this->session->set_userdata('Success', "Record Has Been Successfully Deleted...");
                    redirect(base_url() . 'fyadmin/slider');
                }
            }
            /* end delete */

            $data['page_load'] = $this->get_view();
            $row = $this->query->query("SELECT * FROM `slider` WHERE `accountId` ='" . $id . "' ORDER BY `orderBy` ASC");
            $data['slider'] = $row;
            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/slider', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function add_edit_slider() {
        if ($this->session->userdata('Is_Login') == true) {
            $id = $this->session->userdata('AccountId');
            $type = $this->session->userdata('Account_Type');
            $data['profile_Info'] = $this->custom->profile_Info($id);
            $data['site_Info'] = $this->custom->site_Info();
            /* edit slider */
            $id = $this->input->get('i');
            $delete_id = $this->input->get('id');
            $delete = $this->input->get('delete');
            $edit = $this->input->get('edit');
            if ($edit == "yes") {
                $row = $this->query->query("SELECT * FROM `slider` WHERE `sliderId` ='" . base64_decode($id) . "'");
                $data['slider'] = $row;

                $row = $this->query->query("SELECT * FROM `slider_details` WHERE `sliderId` ='" . $data['slider']['0']->sliderId . "'");
                $data['slider_details'] = $row;

                /* delete slider images */
                if ($delete == "image") {
                    $data = array(
                        'slider' => ''
                    );
                    $table = 'slider';
                    $key = 'sliderId';
                    $query = $this->query->update_query($table, $data, $key, base64_decode($delete_id));
                    if ($query) {
                        $this->session->set_userdata('Success', "Image Has Been Successfully Deleted...");
                        redirect(base_url() . 'fyadmin/slider/add_edit_slider?i=' . $id . '&edit=yes');
                    }
                }
                /* end delete slider images */

                /* delete slider text */
                if ($delete == "text") {
                    $table = 'slider_details';
                    $key = 'detailId';
                    $query = $this->query->delete($table, $key, $delete_id);
                    if ($query) {
                        $this->session->set_userdata('Success', "Record Has Been Successfully Deleted...");
                        redirect(base_url() . 'fyadmin/slider/add_edit_slider?i=' . $id . '&edit=yes');
                    }
                }
                /* end delete slider text */
            }
            /* end edit slider */
            $data['page_active'] = $this->get_view();
            $this->load->view('fyadmin/add_edit_slider', $data);
        } else {
            redirect('fyadmin/');
        }
    }

    public function do_add_slider() {

        $type = $this->input->post('type');
        $slider = $this->input->post('slider');
        $accountId = $this->session->userdata('AccountId');
        $orderBy = $this->input->post('orderBy');
        $created = date("Y-m-d H:i:s");
        $btn = $this->input->post('btn');

        $text = $this->input->post('text');
        $color = $this->input->post('color');
        $size = $this->input->post('size');
        $top = $this->input->post('top');
        $left = $this->input->post('left');
        if ($type == 1) {
            $file_name = 'img-' . $_FILES['slider']['name'];
            $config['upload_path'] = 'assets/upload/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $config['file_name'] = $file_name;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('slider');
            $data = $this->upload->data();
            $slider = $data['file_name'];
        }
        $data = array(
            'type' => $type,
            'slider' => $slider,
            'accountId' => $accountId,
            'orderBy' => $orderBy,
            'status' => 1,
            'created' => $created
        );

        $table = 'slider';
        $query = $this->query->insert_query($table, $data);
        $CreatedID = $query['CreatedID'];

        if ($query['query'] == 1) {

            $count = count($text);

            for ($i = 0; $i < $count; $i++) {
                if (!empty($text[$i])) {
                    $data = array(
                        'text' => $text[$i],
                        'color' => $color[$i],
                        'size' => $size[$i],
                        'top' => $top[$i],
                        'left' => $left[$i],
                        'sliderId' => $CreatedID
                    );
                    $table = 'slider_details';
                    $query = $this->query->insert_query($table, $data);
                }
            }

            $this->session->set_userdata('Success', "Record Has Been Successfully Added...");
            if ($btn == 1) {
                redirect(base_url() . 'fyadmin/slider');
            } else {
                redirect(base_url() . 'fyadmin/slider/add_edit_slider');
            }
        } else {
            $this->session->set_userdata('Error', "Please Try Again...");
            redirect(base_url() . 'fyadmin/');
        }
    }

    public function do_edit_slider() {

        $sliderId = $this->input->post('sliderId');
        $type = $this->input->post('type');
        $slider = $this->input->post('slider');
        $accountId = $this->session->userdata('AccountId');
        $orderBy = $this->input->post('orderBy');
        $updated = date("Y-m-d H:i:s");
        $btn = $this->input->post('btn');

        $text = $this->input->post('text');
        $color = $this->input->post('color');
        $size = $this->input->post('size');
        $top = $this->input->post('top');
        $left = $this->input->post('left');
        if ($type == 1 && !empty($_FILES['slider']['name'])) {
            $file_name = 'img-' . $_FILES['slider']['name'];
            $config['upload_path'] = 'assets/upload/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $config['file_name'] = $file_name;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('slider');
            $data = $this->upload->data();
            $slider = $data['file_name'];
            $data = array(
                'slider' => $slider
            );
            $table = 'slider';
            $key = 'sliderId';
            $query = $this->query->update_query($table, $data, $key, $sliderId);
        }
        $data = array(
            'type' => $type,
            'accountId' => $accountId,
            'orderBy' => $orderBy,
            'status' => 1,
            'updated' => $updated
        );
        $table = 'slider';
        $key = 'sliderId';
        $query = $this->query->update_query($table, $data, $key, $sliderId);

        if ($query) {
            /* delete slider details */
            $table = 'slider_details';
            $key = 'sliderId';
            $query = $this->query->delete($table, $key, base64_encode($sliderId));
            if ($query) {
                $count = count($text);

                for ($i = 0; $i < $count; $i++) {
                    if (!empty($text[$i])) {
                        $data = array(
                            'text' => $text[$i],
                            'color' => $color[$i],
                            'size' => $size[$i],
                            'top' => $top[$i],
                            'left' => $left[$i],
                            'sliderId' => $sliderId
                        );
                        $table = 'slider_details';
                        $query = $this->query->insert_query($table, $data);
                    }
                }
            }
            $this->session->set_userdata('Success', "Record Has Been Successfully Updated...");
            if ($btn == 1) {
                redirect(base_url() . 'fyadmin/slider/');
            } else {
                redirect(base_url() . 'fyadmin/slider/add_edit_slider?i=' . base64_encode($sliderId) . '&edit=yes');
            }
        } else {
            $this->session->set_userdata('Error', "Please Try Again...");
            redirect(base_url() . 'fyadmin/slider/add_edit_slider?i=' . base64_encode($sliderId) . '&edit=yes');
        }
    }

    public function slider_Json() {
        $row = $this->query->query("SELECT * FROM `slider` ORDER BY sliderId DESC");
        $data['slider_Json'] = $row;
        $count=1;
        foreach ($data['slider_Json'] as $sj) {

            if ($sj->type == 1) {
                $slider = base_url() . 'assets/upload/' . $sj->slider;
            } else if ($sj->type == 2) {
               // preg_match('/src="([^"]+)"/', $sj->slider, $match);
              //  $url = $match[1];
                $slider =  $sj->slider;
            } else {
                $slider = "---";
            }

            if ($sj->type == 1) {
                $type = "Image";
            } else if ($sj->type == 2) {
                $type = "Video";
            } else {
                $type = "---";
            }
            $status = '<a class="waves-effect waves-dark btn blue" title="Status" href="' . base_url() . 'fyadmin/slider?i=' . base64_encode($sj->sliderId) . '&ac=' . $sj->status . '&status=yes">';
            if ($sj->status == 1) {
                $status .= '<i class="mdi-action-thumb-down"></i>';
            } else {
                $status .= '<i class="mdi-action-thumb-up"></i>';
            }
            $status .= '</a> ';
            $status .= '<a class="waves-effect waves-dark btn purple" title="Edit" href="' . base_url() . 'fyadmin/slider/add_edit_slider?i=' . base64_encode($sj->sliderId) . '&edit=yes"><i class="mdi-editor-mode-edit"></i></a>
				    <a class="waves-effect waves-dark btn red" title="Delete"  href="' . base_url() . 'fyadmin/slider?i=' . base64_encode($sj->sliderId) . '&delete=yes" onClick="return confirmDelete();"><i class="mdi-action-delete"></i></a>';

            $rows[0] = $count++;
            $rows[1] = $slider;
            $rows[2] = $type;
            $rows[3] = $sj->orderBy;
            $rows[4] = $status;
            $response['aaData'][] = $rows;
        }
        echo json_encode($response, true);
    }

}
