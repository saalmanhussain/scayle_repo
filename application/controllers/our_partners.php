<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class our_partners extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('cart');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }

    public function index() {

        $data = array(
            'user_ip' => $this->input->ip_address(),
            'created' => date("Y-m-d")
        );

        $table = 'current_users';
        $query = $this->query->insert_query($table, $data);

        $data['site_Info'] = $this->custom->site_Info();
        $data['msg'] = "";
        $data['menu'] = $this->query->get_menu();

        $data['GetOurPartners'] = $this->query->get_our_partners();

        $data['OurPartners'] = $this->query->get_all_active_page(3);
        $data['OurPartners_details'] = $this->query->get_all_active_page_details(3);

        $this->load->view('our-partners', $data);
    }

    public function products() {

        $data = array(
            'user_ip' => $this->input->ip_address(),
            'created' => date("Y-m-d")
        );

        $table = 'current_users';
        $query = $this->query->insert_query($table, $data);

        $data['site_Info'] = $this->custom->site_Info();
        $data['msg'] = "";
        $data['menu'] = $this->query->get_menu();

        $data['GetPartners'] = $this->query->get_partner($this->uri->segment(2));
        $data['GetPartnerCategories'] = $this->query->get_partner_categories($this->uri->segment(2));
        $data['GetPartnerProducts'] = $this->query->get_partner_products($this->uri->segment(2));


        $this->load->view('products', $data);
    }

}
