<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact_us extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('cart');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('fyadmin/query');
        $this->load->library('Custom');
    }

    public function index() {
        
        $data = array(
            'user_ip' => $this->input->ip_address(),
            'created' => date("Y-m-d")
        );

        $table = 'current_users';
        $query = $this->query->insert_query($table, $data);

        $data['site_Info'] = $this->custom->site_Info();
        $data['msg'] = "";
        $data['menu'] = $this->query->get_menu();
	   
	    $data['slider']      = $this->query->get_all_active_slider();
	    $data['OurPartners'] = $this->query->get_our_partners();

     	$data['ContactUs'] = $this->query->get_all_active_page(5);
        $data['ContactUs_details'] = $this->query->get_all_active_page_details(5);

        $this->load->view('contact-us', $data);
    }

	
	public function contact_msg()
    {
        $site_Info = $this->custom->site_Info();
        
        $name     = $this->input->post('contactName');
        $email    = $this->input->post('email');
        $msg      = $this->input->post('message');
        
		$to1      = $email;	
		$subject1 = $site_Info['Title']." Inquiry";
		$txt1     = "[AUTO-RESPONSE, please do not reply]\n\n\nDear Customer,\n\nIt is to confirm that we have received your inquiry. Our representative\nwill respond to your inquiry shortly.\n\nRegards,\n\n".$site_Info['Title'];
		$from2    = $site_Info['EmailFrom'];
		$headers  = "From: ".$site_Info['Title']." <$from2>" . "\r\n";

		mail($to1,$subject1,$txt1,$headers);
        
        $to2      = $site_Info['EmailTo'];	
	    $subject2 = $site_Info['Title']." Inquiry";
	    $txt2     = "Name: $name \nEmail: $email\nMessage: $msg";
	    $from2    = $site_Info['EmailFrom'];
        $headers  = "From: ".$site_Info['Title']." <$from2>" . "\r\n";        
        
		if (mail($to2,$subject1,$txt1,$headers)){
			$this->session->set_userdata('Success',"Thanks for your message. We will be in touch with you soon.");
			redirect(base_url().'contact-us');
		} else {
			$this->session->set_userdata('Error',"Please try again.");
			redirect(base_url().'contact-us');
		}
    }

}
