<?php
class Custom {
	
	private $CI;
	
	 function __construct() {
       $this->CI =& get_instance();
       $this->CI->load->database();
	 }
	
  		public function seo_Url($string){
			
			$url = str_replace("'", '', $string);
			$url = str_replace('%20', ' ', $url);
			$url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); 
			// substitutes anything but letters, numbers and '_' with separator
			$url = trim($url, "-");
			$url = iconv("utf-8", "us-ascii//TRANSLIT", $url); 
			// you may opt for your own custom character map for encoding.
			$url = strtolower($url);
			$url = preg_replace('~[^-a-z0-9_]+~', '', $url); 
			// keep only letters, numbers, '_' and separator
			return $url;
		}
		
	   public function site_Info(){
		  
		  $query = $this->CI->db->query('SELECT * FROM `settings` WHERE `settingsId` = "1"');
		  $query = $query->result();
			
		  foreach ($query as $row){
				
					$data['Title'] 			 = $row->title;
					$data['MetaKeywords'] 	 = $row->metaKeyword;
					$data['MetaDescription'] = $row->metaDescription;
					$data['HeaderText'] 	 = $row->headerText;
					$data['FooterText'] 	 = $row->footerText;
					$data['Logo'] 	 		 = $row->logo;
					$data['EmailTo'] 	 	 = $row->emailTo;
					$data['EmailFrom'] 	 	 = $row->emailFrom;
					$data['Phone'] 	 		 = $row->phone;
					$data['Mobile'] 	 	 = $row->mobile;
					$data['Copyright'] 	 	 = $row->copyright;
					
					$data['Facebook'] 	 	 = $row->Facebook;
					$data['Twitter'] 	 	 = $row->Twitter;
					$data['GooglePlus'] 	 = $row->GooglePlus;
					$data['LinkedIn'] 	 	 = $row->LinkedIn;
					
					$data['Address'] 	 	 = $row->address;
					$data['text_1'] 	 	 = $row->text_1;
					$data['text_2'] 	 	 = $row->text_2;
					$data['text_3'] 	 	 = $row->text_3;
					
				
			}
		 return $data;
		
		}
		
		
		 public function profile_Info($ID){
		  
		  $query = $this->CI->db->query("SELECT * FROM `accounts` join account_details on account_details.accountId = accounts.accountId WHERE accounts.accountId =".$ID);
		  $query = $query->result();
			
		  foreach ($query as $row){
			$data['Profile_ID'] 		= $row->accountId;
			$data['Profile_Name'] 		= $row->name;
			$data['Profile_Email'] 		= $row->email;
			$data['Profile_Pic'] 		= $row->profile_pic;
			}
		 return $data;
		
		}
}
?>