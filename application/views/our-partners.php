<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<!--  Basic Page Needs -->
	<meta charset="UTF-8" />
    <title><?=$OurPartners[0]->title ." | ". $site_Info['Title']?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>"/>
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>"/>
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?=$OurPartners[0]->title ." | ". $site_Info['Title']?>" />
	<meta property="og:description" content="<?=$site_Info['MetaDescription']?>" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:site_name" content="<?=$site_Info['Title']?>" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="<?=$site_Info['MetaDescription']?>"/>
	<meta name="twitter:title" content="<?=$OurPartners[0]->title ." | ". $site_Info['Title']?>"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?=PATH?>favicon.ico" />
	
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>style.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/grid.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/fonts.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/flexslider.css" />

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="<?=PATH?>css/ie8.css" />
	<![endif]-->

</head>

<body class="home">
	<div id="page" class="hfeed site">

		<div class="page-decoration">
			
			<!-- Header -->
			<?php $this->load->view('include/inc_header')?>

			<img src="<?=PATH?>upload/<?=$OurPartners_details[0]->Image?>" alt="Custom Bg" class="decoration">


		</div>

		<div class="container_12 clearfix">

			<!-- Page Content -->
			<div class="grid_12 no-m-b">
				<section class="page classes grid">
					<div class="page-header">
						<h1 class="title"><?=$OurPartners[0]->heading?></h1>
					</div>
					<div class="page-content">
						
					<?php 
						$color_count = 0;
						foreach($GetOurPartners as $ourPartners){
							$color_count++;
							if($color_count == 1){
								$color = "red";
							}else if($color_count == 2){
								$color = "blue";
							}else{
								$color = "";
							}
					?>		
						<div class="item grid_4">
							<div class="item-image">
								<img src="<?=PATH?>upload/<?=$ourPartners->profile_pic?>"  width="360" height="180"  alt="<?=$ourPartners->name?>">
								<a title="Outdoor Biking" href="our-partners/<?=$ourPartners->seo?>" class="overlay">
									<h3><?=$ourPartners->name?></h3>
								</a>
							</div>
						</div>
					<?php 
						}
					?>
						
					</div>
				</section>
			</div>
		</div>

		<!-- Footer -->
		<?php $this->load->view('include/inc_footer')?>
			
	</div>
		
	<!-- Scripts -->
	<script src="<?=PATH?>js/jquery-1.10.2.min.js"></script>
	<script src="<?=PATH?>js/jquery.flexslider.js"></script>
	<script src="<?=PATH?>js/masonry.pkgd.js"></script>
	<script src="<?=PATH?>js/jquery.meanmenu.js"></script>
	<script defer src="<?=PATH?>js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="<?=PATH?>js/base.js"></script>

	</body>
</html>