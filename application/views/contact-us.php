<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<!--  Basic Page Needs -->
	<meta charset="UTF-8" />
    <title><?=$ContactUs[0]->title ." | ". $site_Info['Title']?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>"/>
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>"/>
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?=$ContactUs[0]->title ." | ". $site_Info['Title']?>" />
	<meta property="og:description" content="<?=$site_Info['MetaDescription']?>" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:site_name" content="<?=$site_Info['Title']?>" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="<?=$site_Info['MetaDescription']?>"/>
	<meta name="twitter:title" content="<?=$ContactUs[0]->title ." | ". $site_Info['Title']?>"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?=PATH?>favicon.ico" />
	
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>style.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/grid.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/fonts.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/flexslider.css" />

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="<?=PATH?>css/ie8.css" />
	<![endif]-->

</head>

<body class="home">
	<div id="page" class="hfeed site">

		<div class="page-decoration">
			
			<!-- Header -->
			<?php $this->load->view('include/inc_header')?>
				
			<img src="<?=PATH?>upload/<?=$ContactUs_details[0]->Image?>" alt="Custom Bg" class="decoration">


		</div>

		<div class="container_12 clearfix">

			<!-- Page Content -->
			<div class="grid_8 no-m-b">
				<article class="page single">
					<div class="page-header">
						<h1 class="title"><?=$ContactUs[0]->heading?></h1>
					</div>
					<div class="page-content singlepage">
						<div class="item-content">
								<?=$ContactUs[0]->description?>
					<?php
                    if($this->session->userdata('Success')){
                        echo '<div class="alert green">'.$this->session->userdata('Success').'</div>';
                        $this->session->unset_userdata('Success');
                    }
                    
                    if($this->session->userdata('Error')){
                        echo '
                        <div class="alert red"> '.$this->session->userdata('Error').'</div>';
                        $this->session->unset_userdata('Error');
                    }
                    ?>
								<form action="contact_us/contact_msg" id="contact-form" method="post">

									<p>
										<label for="contactName"></label>
										<input type="text" name="contactName" id="contactName" value="" placeholder="Name*" required/>
										<span class="clear"></span>
									</p>
									<p>
										<label for="email"></label>
										<input type="email" name="email" id="email" value="" placeholder="Email Adress*" required/>
										<span class="clear"></span>
									</p>
									<p>
										<label for="commentsText"></label>
										<textarea class="contactme-text required requiredField radius" name="message" cols="10" rows="10" placeholder="Message" required="required"></textarea>
										<span class="clear"></span>
									</p>
									<p>
										<input id="submit" value="Send !" type="submit"></input>
										<input type="hidden" name="submitted" id="submitted" value="true" />
									</p>
								</form>
						</div>
					</div>
				</article>

			</div>

			<!-- Page Sidebar -->
			<div class="sidebar widget-area grid_4" role="complementary">
				<aside class="widget lightorange grid_4">
					<div class="widget-header">
						<h3 class="title">Contact Infomation</h3>
					</div>
					<address class="vcard">
						<span><?=$site_Info['Address']?></span>
						<br/>
						<span>Phone: +<?=$site_Info['Phone']?></span>
						<br/>
						<span>Mobile: +<?=$site_Info['Mobile']?></span>
						<br/>
						<span>E-mail: <?=$site_Info['EmailTo']?></span>
						<br/>
						<span>Website: <?=base_url()?></span>
					</address>
				</aside>
				<!--<aside id="text-3" class="widget widget_text">
					<div class="widget-header">
						<h3 class="title">About Us</h3>
					</div>

					<div class="textwidget">On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue
					</div>
				</aside>-->
			</div>
		</div>

		<!-- Footer -->
		<?php $this->load->view('include/inc_footer')?>
			
	</div>
		
	<!-- Scripts -->
	<script src="<?=PATH?>js/jquery-1.10.2.min.js"></script>
	<script src="<?=PATH?>js/jquery.flexslider.js"></script>
	<script src="<?=PATH?>js/masonry.pkgd.js"></script>
	<script src="<?=PATH?>js/jquery.meanmenu.js"></script>
	<script defer src="<?=PATH?>js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="<?=PATH?>js/base.js"></script>

	</body>
</html>