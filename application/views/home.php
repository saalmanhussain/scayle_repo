<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<!--  Basic Page Needs -->
	<meta charset="UTF-8" />
    <title><?=$Home[0]->title ." | ". $site_Info['Title']?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>"/>
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>"/>
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?=$Home[0]->title ." | ". $site_Info['Title']?>" />
	<meta property="og:description" content="<?=$site_Info['MetaDescription']?>" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:site_name" content="<?=$site_Info['Title']?>" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="<?=$site_Info['MetaDescription']?>"/>
	<meta name="twitter:title" content="<?=$Home[0]->title ." | ". $site_Info['Title']?>"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?=PATH?>favicon.ico" />
	<link rel="apple-touch-icon" href="apple-touch-icon.html" />
	
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>style.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/grid.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/fonts.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?=PATH?>css/flexslider.css" />

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="<?=PATH?>css/ie8.css" />
	<![endif]-->

</head>

<body class="home">
	<div id="page" class="hfeed site">

		<div class="page-decoration">
			
			<!-- Header -->
			<?php $this->load->view('include/inc_header')?>

			<!-- Slider -->
			<div class="flexslider loading">
				<ul class="slides">
			<?php 
				foreach($slider as $slider){
					if($slider->type == "1"){
			?>
					<li>
						<img src="<?=PATH?>upload/<?=$slider->slider?>" alt="image"/>
						<div class="flex-caption">
							<div class="container_12">
								<div class="grid_6 right">
									<div class="flex-content">
										<h1><?=$slider->text?></h1>
										<hr/>
										<p><?=$slider->color?></p>
										<a title="<?=$slider->size?>" href="<?=$slider->top?>" class="btn"><?=$slider->size?></a>
									</div> 
								</div>
							</div>
						</div>
					</li>
			<?php	
						}	
				}	
			?>		
				</ul>
			</div>

		</div>

		<!-- Announcement -->
		<div class="announcement">
			<div class="container_12">
				<aside>
					<div class="grid_12">
						<?=$Home[0]->description?>
					</div>
				</aside>
			</div>
		</div>

		<!-- First Widget Area -->
		<div class="widget-area first-widget-area container_12 clearfix" role="complementary">
			<div class="grid_12">
				<aside class="widget classes grid">
					<div class="widget-header">
						<h3 class="title">Restaurants</h3>
						<a title="Our Partners" href="our-partners" class="view-all icon-arrow-right right"></a>
					</div>
					
					<div class="widget-content">
					
					<?php 
						$color_count = 0;
						foreach($OurPartners as $ourPartners){
							$color_count++;
							if($color_count == 1){
								$color = "red";
							}else if($color_count == 2){
								$color = "blue";
							}else{
								$color = "";
							}
					?>		
						<div class="item grid_4">
							<div class="item-image">
								<img src="<?=PATH?>upload/<?=$ourPartners->profile_pic?>"  width="360" height="180"  alt="<?=$ourPartners->name?>">
								<a title="<?=$ourPartners->name?>" href="our-partners/<?=$ourPartners->seo?>" class="overlay">
									<h3><?=$ourPartners->name?></h3>
								</a>
							</div>
						</div>
					<?php 
							if($color_count == 3){
								break;
							}
						}
					?>	
						
					</div>
				</aside>
				
				
			</div>
		</div>

		<!-- Footer -->
		<?php $this->load->view('include/inc_footer')?>
			
	</div>
		
	<!-- Scripts -->
	<script src="<?=PATH?>js/jquery-1.10.2.min.js"></script>
	<script src="<?=PATH?>js/jquery.flexslider.js"></script>
	<script src="<?=PATH?>js/masonry.pkgd.js"></script>
	<script src="<?=PATH?>js/jquery.meanmenu.js"></script>
	<script defer src="<?=PATH?>js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="<?=PATH?>js/base.js"></script>

	</body>
</html>