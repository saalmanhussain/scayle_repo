<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <!--  Basic Page Needs -->
        <meta charset="UTF-8" />
        <title><?= $GetPartners[0]->name . " | " . $site_Info['Title'] ?></title>
        <meta name="description" content="<?= $site_Info['MetaDescription'] ?>"/>
        <meta name="keywords" content="<?= $site_Info['MetaKeywords'] ?>"/>
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?= " | " . $site_Info['Title'] ?>" />
        <meta property="og:description" content="<?= $site_Info['MetaDescription'] ?>" />
        <meta property="og:url" content="<?= base_url() ?>" />
        <meta property="og:site_name" content="<?= $site_Info['Title'] ?>" />
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:description" content="<?= $site_Info['MetaDescription'] ?>"/>
        <meta name="twitter:title" content="<?= " | " . $site_Info['Title'] ?>"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?= PATH ?>favicon.ico" />

        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" media="all" href="<?= PATH ?>style.css" />
        <link rel="stylesheet" type="text/css" media="all" href="<?= PATH ?>css/grid.css" />
        <link rel="stylesheet" type="text/css" media="all" href="<?= PATH ?>css/fonts.css" />
        <link rel="stylesheet" type="text/css" media="all" href="<?= PATH ?>css/flexslider.css" />
    
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="<?= PATH ?>css/ie8.css" />
        <![endif]-->

    </head>

    <body class="home">
        <div id="page" class="hfeed site">

            <div class="page-decoration">

                <!-- Header -->
                <?php $this->load->view('include/inc_header') ?>

                <img src="<?= PATH ?>upload/bg.jpg" alt="Custom Bg" class="decoration">


            </div>
            <div class="row">
                <div class="container_12 clearfix">

                    <!-- Page Content -->
                    <div class="grid_8 no-m-b">
                        <article class="page single">
                            <div class="page-header">
                                <h1 class="title">Our Products</h1>
                            </div>
                            <div class="page-content singlepage">
                                <div class="item-content">

                                    <?php foreach ($GetPartnerCategories as $GPC) { ?>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th colspan="3"><?= $GPC->title ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($GetPartnerProducts as $GPP) {
                                                    if ($GPP->cat_id == $GPC->catID) {
                                                        ?>
                                                        <tr>
                                                            <td width="60%"><?= $GPP->title ?><br><small><?= $GPP->SubText ?></small></td>
                                                            <td width="20%"><?php if ($GPP->Price) { ?><small>Rs.</small><?= $GPP->Price ?>/=<?php } ?><?php if ($GPP->OldPrice) { ?><br><small><del>Rs.<?= $GPP->OldPrice ?></del></small><?php } ?></td>
                                                            <td width="20%"><a class="button small grey square" onclick="add_cart('<?= $GPP->proID ?>','<?= $GPP->Price ?>','1','<?= $GPP->accountId_created ?>','<?= $GPP->title ?>')" >Add to Cart</a></td>
                                                        </tr>
                                                    <?php }
                                                    
                                                    $p_id = $GPP->accountId_created;
                                                }
                                                ?>  
                                            </tbody>
                                        </table>
                                    <?php } ?>

                                    <?php
                                    if ($this->session->userdata('Success')) {
                                        echo '<div class="alert green">' . $this->session->userdata('Success') . '</div>';
                                        $this->session->unset_userdata('Success');
                                    }

                                    if ($this->session->userdata('Error')) {
                                        echo '
                            <div class="alert red"> ' . $this->session->userdata('Error') . '</div>';
                                        $this->session->unset_userdata('Error');
                                    }
                                    ?>
                                </div>
                            </div>
                        </article>

                    </div>

                    <div class="grid_4 no-m-b">
                        <article class="page single">
                            <div class="page-header">
                                <h1 class="title">Contact Infomation</h1>
                            </div>
                            <div class="page-content singlepage">
                                <div class="item-content">

                                    <address class="vcard">
                                        <span><strong>Business Name:</strong> <?= $GetPartners[0]->name ?></span>
                                        <br>
                                        <span><strong>Email Address:</strong> <?= $GetPartners[0]->email ?></span>
                                        <br/>
    <?php if ($GetPartners[0]->phone) { ?>
                                            <span><strong>Phone Number:</strong> +<?= $GetPartners[0]->phone ?></span>
                                            <br/>
                                        <?php } ?>
    <?php if ($GetPartners[0]->address) { ?>
                                            <span><strong>Address:</strong> <?= $GetPartners[0]->address ?>, <?= $GetPartners[0]->city ?>, <?= $GetPartners[0]->state ?>, <?= $GetPartners[0]->country ?></span>
                                            <br/>
    <?php } ?>

                                    </address>
                                    <div class="share-items">
                                        <?php if ($GetPartners[0]->Facebook) { ?>
                                            <a href="<?= $GetPartners[0]->Facebook ?>" target="_blank" class="icon-facebook"></a>
                                        <?php } ?>
                                        <?php if ($GetPartners[0]->Twitter) { ?>
                                            <a href="<?= $GetPartners[0]->Twitter ?>" target="_blank" class="icon-twitter"></a>
                                        <?php } ?>
                                        <?php if ($GetPartners[0]->Googleplus) { ?>
                                            <a href="<?= $GetPartners[0]->Googleplus ?>" target="_blank" class="icon-google-plus"></a>
                                        <?php } ?>
                                        <?php if ($GetPartners[0]->LinkedIn) { ?>
                                            <a href="<?= $GetPartners[0]->LinkedIn ?>" target="_blank" class="icon-linkedin"></a>
    <?php } ?>
                                    </div>	
                                </div>
                            </div>
                        </article>
						<br><br><br>
						<div>&nbsp;</div>
						
						<article class="page single">
                            <div class="page-header">
                                <h1 class="title">Cart</h1>
                            </div>
                            <div class="page-content singlepage">
                                <div class="item-content">
									<table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="50%">Product</th>
                                                    <th width="10%">Qty</th>
                                                    <th width="10%">Price</th>
                                                    <th width="10%" colspan="3">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="cart_<?=$p_id?>">
                                                 
                                            </tbody>
                                        </table>
										<?php if($this->cart->contents()){?>
                                        <a data-toggle="modal" data-target="#check_out" class="button small green square">Check Out</a>
                                    	<?php }?>
                                </div>
                            </div>
                        </article>
                    </div>
					
                </div>
            </div>
            <div class="row">
                <div class="container_12 clearfix">
                    <div class="modal fade" id="check_out" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×
                                    </button>
                                    <h4 class="modal-title" >Check Out</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                            <div class="" style="border-radius: 0; box-shadow: none;">
                                                <div id="checkout_error" class="alert red" style="display:none"></div>
                                                <div id="checkout_sucess" class="alert green" style="display:none"></div>
                                                <form id="contact-form" method="post">
                                                    <p>
                                                        <label for="contactName"></label>
                                                        <input type="text" name="checkout_name" id="contactName" value="" placeholder="Name*" required/>
                                                        <span class="clear"></span>
                                                    </p>
                                                    <p>
                                                        <label for="email"></label>
                                                        <input type="email" name="checkout_email" id="email" value="" placeholder="Email Adress*" required/>
                                                        <span class="clear"></span>
                                                    </p>
                                                    <p>
                                                        <label for="email"></label>
                                                        <input type="text" name="checkout_phone" id="email" value="" placeholder="phone Number*" required/>
                                                        <span class="clear"></span>
                                                    </p>
                                                    <p>
                                                        <label for="email"></label>
                                                        <input type="text" name="checkout_address" id="email" value="" placeholder="Address*" required/>
                                                        <span class="clear"></span>
                                                    </p>
                                                    <input type="hidden" name="partner_id" value="<?= $p_id ?>">
                                                    <p>
                                                        <input id="submit" class="checkout_form" value="checkout" type="submit"/>
                                                    </p>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    



                </div>
            </div>
            <!-- Footer -->
<?php $this->load->view('include/inc_footer') ?>

        </div>

        <!-- Scripts -->
        <script src="<?= PATH ?>js/jquery-1.10.2.min.js"></script>
        <script src="<?= PATH ?>js/jquery.flexslider.js"></script>
        <script src="<?= PATH ?>js/masonry.pkgd.js"></script>
        <script src="<?= PATH ?>js/jquery.meanmenu.js"></script>
        <script defer src="<?= PATH ?>js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="<?= PATH ?>js/base.js"></script>
        <script src="<?= PATH ?>js/bootstrap.min.js"></script>
    
        <script>
        
        function add_cart(id, price, qty_1, userId,name) {
            var id = id;
            var p_price = price;
            var qty = qty_1;

            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>home/add_cart/",
                data: {'id': id, 'price': p_price, 'qty': qty, 'user_id': userId,'name': name},
                success: function (result) {
                    //alert(result);
                    
                    if (result != "-1") {
                        //alert('Product Added Successfully.');

                        $('#cart_'+userId).html(result);
                        //$('.success_message').html('<i class="fa fa-check-circle"></i> Product Added Successfully');
                        //$('.success_message').fadeIn('slow');
                        //setTimeout(function () {
                        //    $('.success_message').fadeOut('slow');
                        //}, 2000); // <-- time in milliseconds
                    }
                    else {

                        //$('.error_message').html('<i class="fa fa-close"></i> Please Try Again');
                        //$('.error_message').fadeIn('slow');
                        //setTimeout(function () {
                        //    $('.error_message').fadeOut('slow');
                        //}, 2000); // <-- time in milliseconds
                    }
                }
            });
        }
        
        function remove_cart_item(id, partner_id) {
            var pId = id;

            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>home/remove_cart_item",
                data: {
                    'pId': pId,
                    'partner_id': partner_id
                },
                success: function (result) {
                    if (result >= 0) {


                        $('#cart_'+partner_id).html(result);
                    }
                    else {
                        $('#cart_'+partner_id).html(result);
                    }
                }
            });
        }
        
        function subtract_cart_item(id, partner_id) {
        
            var pId = id;

            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>home/subtract_cart_item",
                data: {
                    'pId': pId,
                    'partner_id': partner_id
                },
                success: function (result) {
                    if (result >= 0) {
                        $('#cart_'+partner_id).html(result);
                    }
                    else {
                        $('#cart_'+partner_id).html(result);
                    }
                }
            });
        }
        
        function add_cart_item(id, partner_id) {
        
            var pId = id;

            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>home/add_cart_item",
                data: {
                    'pId': pId,
                    'partner_id': partner_id
                },
                success: function (result) {
                    if (result >= 0) {


                        $('#cart_'+partner_id).html(result);
                    }
                    else {
                        $('#cart_'+partner_id).html(result);
                    }
                }
            });
        }
        
        $( document ).ready(function() {
            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>home/header_cart_view/<?= $p_id ?>",
                success: function (result) {
                    //alert(result);
                    
                    if (result != "-1") {
                        //alert('Product Added Successfully.');

                        $('#cart_'+<?= $p_id ?>).html(result);
                        //$('.success_message').html('<i class="fa fa-check-circle"></i> Product Added Successfully');
                        //$('.success_message').fadeIn('slow');
                        //setTimeout(function () {
                        //    $('.success_message').fadeOut('slow');
                        //}, 2000); // <-- time in milliseconds
                    }
                    else {

                        //$('.error_message').html('<i class="fa fa-close"></i> Please Try Again');
                        //$('.error_message').fadeIn('slow');
                        //setTimeout(function () {
                        //    $('.error_message').fadeOut('slow');
                        //}, 2000); // <-- time in milliseconds
                    }
                }
            });
            
            $( ".checkout_form" ).click(function() {
                
                var count = 0;
                var checkout_name = $("input[name=checkout_name]").val();
                var checkout_email = $("input[name=checkout_email]").val();
                var checkout_phone = $("input[name=checkout_phone]").val();
                var partner_id = $("input[name=partner_id]").val();
                var checkout_address = $("input[name=checkout_address]").val();
                
                if(checkout_name == "")
                {
                    alert("Name Required");
                    count = 1;
                }
                
                if(checkout_email == "")
                {
                    alert("Email Required");
                    count = 1;
                }
                
                if(checkout_phone == "")
                {
                    alert("Phone Required");
                    count = 1;
                }
                
                if(count == 0)
                {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url() ?>home/checkout_form",
                        data: {
                            'checkout_name': checkout_name,
                            'checkout_email': checkout_email,
                            'checkout_phone': checkout_phone,
                            'partner_id': partner_id,
                            'checkout_address': checkout_address
                        },
                        success: function (result) {
                            
                            if (result == "1") {
                                $('#checkout_sucess').html('<i class="fa fa-check-circle"></i> Checkout Successfully');
                                $('#checkout_sucess').show();
                                $('#contact-form')[0].reset();
                            }
                            else {
                                $('#checkout_error').html('<i class="fa fa-check-circle"></i> Please Try Again');
                                $('#checkout_error').show();
                                $('#contact-form')[0].reset();
                            }
                        }
                    });
                }
                
                return false;
            });
        });
        </script>
    </body>
</html>
