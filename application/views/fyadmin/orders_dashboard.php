<div class="row">
                <div class="col s12 12">
                    <table id="table4" class="display table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                 <th>#</th>
                                <th>Order Numer</th>
                                <th>Items</th>
								<th>Customer</th>
                                <th>Dealer</th>
                                <th>Created</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>   
                        </thead>
                        <tbody>
                            <?php $count = 1; foreach($order as $row){ ?>
                            <tr <?php if($row->view_order=="1"){echo 'class="red lighten-5"';}?>>
                                <td><?=$count++ ?></td>
                                <td><?=$row->orderNo?></td>
                                <td><?=$row->products ?></td>
                                <td><?=$row->customer ?></td>
                                <td><?=$row->dealer ?></td>
                                <td><?=$row->created ?></td>
                                <td>
								
								
                                        <?php
                                        if ($row->status == 0) {
                                            echo "In Progress";
                                        } else if ($row->status == 1) {
                                            echo "Success";
                                        } else {
                                            echo "Cancel";
                                        }
                                        ?>
										<?php if($row->view_order=="1"){?>
										<script>
										document.getElementById('myaudio').play();
										Materialize.toast('<a href="<?=base_url()?>fyadmin/accounts/order_details?id=<?=base64_encode($row->orderId)?>">Order Numer: <?=$row->orderNo?></a>', 4000);
										</script>	
										<?php }?>
                                </td>
                                <td><a href='<?=base_url()?>fyadmin/accounts/order_details?id=<?= base64_encode($row->orderId) ?>' class="waves-effect waves-dark btn blue">Details</a></td>
                            </tr> 
                            <?php if($count==10){
										break;
									}
							} ?>
                        </tbody>
                    </table>
                </div>
            </div>