<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $site_Info['Title'] ?></title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

        <link rel="icon" type="image/png" href="<?= PATH_ADMIN ?>assets/_con/images/icon.png">

        <!-- nanoScroller -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/nanoScroller/nanoscroller.css" />


        <!-- FontAwesome -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/font-awesome/css/font-awesome.min.css" />

        <!-- Material Design Icons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/material-design-icons/css/material-design-icons.min.css" />

        <!-- IonIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/ionicons/css/ionicons.min.css" />

        <!-- WeatherIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/weatherIcons/css/weather-icons.min.css" />

        <!-- Google Prettify -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/google-code-prettify/prettify.css" />
        <!-- Main -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/_con/css/_con.min.css" />

        <!--[if lt IE 9]>
          <script src="<?= PATH_ADMIN ?>assets/html5shiv/html5shiv.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<?= PATH ?>ckeditor/ckeditor.js"></script>
    </head>

    <body>



        <!-- /Top Navbar -->
        <?= $this->load->view('fyadmin/top-navbar'); ?>	
        <!-- /Top Navbar -->

        <!-- Sidebar -->
        <?= $this->load->view('fyadmin/sidebar'); ?>	
        <!-- /.sidebar -->



        <!-- Main Content -->
        <section class="content-wrap">


            <!-- Breadcrumb -->
            <div class="page-title">

                <div class="row">
                    <div class="col s12 m9 l10">
                        <h1>
                            Order Details
                        </h1>
                        <ul>
                            <li>
                                <a href="<?= base_url() ?>fyadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>fyadmin/accounts/orders"> View All</a> 
                            </li>

                        </ul>
                    </div>
                    <div class="col s12 m3 l2 right-align">
                        <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
                    </div>
                </div>

            </div>
            <!-- /Breadcrumb -->

            <?php
            if ($this->session->userdata('Success')) {
                echo '
			<div class="alert green lighten-4 green-text text-darken-2">
			' . $this->session->userdata('Success') . '
			</div>
			<br>';
                $this->session->unset_userdata('Success');
            }

            if ($this->session->userdata('Error')) {
                echo '
				<div class="alert">
				  ' . $this->session->userdata('Error') . '
				</div>
		    </div>
			<br>';
                $this->session->unset_userdata('Error');
            }
            { ?>
            <form action="<?= base_url() ?>fyadmin/accounts/send_email" method="post" enctype="multipart/form-data" data-parsley-validate>
                    <div class="card-panel">
                       
                        <!-- Text Field -->
                        <div class="row">
                            <div class="col l12">
                                      <div class="btn col l12">
                                        <strong> Order Status: 
                                        <?php
                                        if ($order_detail[0]->status == 0) {
                                            echo "<span>In Progress</span>";
                                        } else if ($order_detail[0]->status == 1) {
                                            echo "<span>Success</span>";
                                        } else {
                                            echo "<span>Cancel</span>";
                                        }
                                        ?>
                                        </strong>
<br>
                                    </div>
                             </div>
                        </div>
                        <!-- Text Field -->
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="orderId" name="orderId" type="hidden" readonly="" value="<?= $order_detail[0]->orderId ?>" class="validate" required="">
                                    <input id="title" name="email" type="text" readonly="" value="<?= $order_detail[0]->email ?>" class="validate" required="">
                                    <label for="title">Email</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" readonly=""  value="<?= $order_detail[0]->orderNo ?>" class="validate" required="">
                                    <label for="title">Order Number</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" readonly=""  value="<?= $order_detail[0]->products ?>" class="validate" required="">
                                    <label for="title">Products</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" readonly=""  value="<?= $order_detail[0]->items ?>" class="validate" required="">
                                    <label for="title">Items</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" readonly=""  value="$ <?= $order_detail[0]->price ?>" class="validate" required="">
                                    <label for="title">Price</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" readonly="" value="<?= $order_detail[0]->customer ?>" class="validate" required="">
                                    <label for="title">Customer</label>
                                </div>
                            </div>
                        </div>
                        
						<div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" readonly="" value="<?= $order_detail[0]->c_email ?>" class="validate" required="">
                                    <label for="title">Customer Login Email</label>
                                </div>
                            </div>
                        </div>
						
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <input id="title" name="title" type="text" readonly="" value="<?= $order_detail[0]->dealer ?>" class="validate" required="">
                                    <label for="title">Dealer</label>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Text Field -->
                        <div class="row" id="Video">
                            <div class="col l2 l12">
                                <div class="input-field">
                                    <textarea id="description" name="email_text" class="materialize-textarea">
					
<p>Dear User, </p>
<p>Here are the details of your account that you purchased.</p>
 <p>Order Number:  </p>
 <p>
                                        <?php
                                        $products = explode(",",$order_detail[0]->item_id);
                                        for($i = 0; $i < count($products) ; $i++)
                                        {
                                        ?>
                                       Username<?= $i+1 ?> : <?= $this->menu->product_dealer_name($products[$i]) ?><br />
                                        
                                        <?php
                                        }
                                        ?>
  Password: <br />
  Family Pin:<br />
  <br />
  Note: Steam guard is disabled for this account.</p>
<p><strong>Procedure and FAQ:</strong><br />
<ol type="i">
  <li>Change the ownership of the account.</li>
  	<ol type="a">
  		<li>After putting the user and password, go to steam at the top right and then select Settings >> Account >> CHANGE CONTACT EMAIL ADDRESS.</li>
  		<li>Now change the password settings >> Account >> CHANGE PASSWORD.</li>
  		<li>After this, Enable the steam guard Settings  >> MANAGE STEAM GUARD ACCOUNT SECURITY and Enable it.<br />
  		  <br />
  		</li>
    </ol>
  <li>How to Enable/Disable family view</li>
    <ol type="a">
  		<li>To Enable/Disable, Steam >> Settings >> Family >> Manage Family view.<br />
  		  <br />
  		</li>
  	</ol>
   <li>How to Contact us?</li>
   <ol type="a">
  		<li>It’s just simple, just login in site and contact us via live chat/call.</li>
  </ol>  
</ol> 

<p>&nbsp;</p>
<p>Regards<br />
  
  Zerox Staff
</p>								
	</textarea>
                                    <script type="text/javascript">
                                        CKEDITOR.replace('description',
                                                {
                                                    filebrowserBrowseUrl: '<?= PATH ?>ckfinder/ckfinder.html',
                                                    filebrowserImageBrowseUrl: '<?= PATH ?>ckfinder/ckfinder.html?Type=Images',
                                                    filebrowserFlashBrowseUrl: '<?= PATH ?>ckfinder/ckfinder.html?Type=Flash',
                                                    filebrowserUploadUrl: '<?= PATH ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                                    filebrowserImageUploadUrl: '<?= PATH ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                                    filebrowserFlashUploadUrl: '<?= PATH ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                                                });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <!-- /Text Field -->

                        <!-- Text Field -->
                        <div class="row">
                            <div class="col l2 l12">
                                <div class="input-field">
                                    <button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="1">Send</button>        
                                </div>
                            </div>
                        </div>
                        <!-- /Text Field -->



                    </div> 

                </form>
<?php } ?>
        </section>
        <!-- /Main Content -->

        <!-- Search Bar -->
<?= $this->load->view('fyadmin/search_bar'); ?>	
        <!-- /Search Bar -->




        <!--Chat-->
<?= $this->load->view('fyadmin/chat'); ?>	
        <!-- /Chat -->


        <!-- footer -->
<?= $this->load->view('fyadmin/footer'); ?>	
        <!-- /.footer -->
        <!-- DEMO [REMOVE IT ON PRODUCTION] -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_demo.js"></script>

        <!-- jQuery -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery/jquery.min.js"></script>

        <!-- jQuery RAF (improved animation performance) -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

        <!-- nanoScroller -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

        <!-- Materialize -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/materialize/js/materialize.min.js"></script>

        <!-- Sortable -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/sortable/Sortable.min.js"></script>

        <!-- Main -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_con.min.js"></script>


        <!-- Google Prettify -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/google-code-prettify/prettify.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/parsley/parsley.min.js"></script>
    </body>
    <script>
                                    function confirmDelete() {
                                        return confirm("Are you sure you want to delete this?");
                                    }
    </script>
</html>