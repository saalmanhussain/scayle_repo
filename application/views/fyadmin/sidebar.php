<aside class="yaybar yay-shrink yay-hide-to-small yay-gestures yay-light yay-static">

    <div class="top">
        <div>
            <!-- Sidebar toggle -->
            <a href="#" class="yay-toggle">
                <div class="burg1"></div>
                <div class="burg2"></div>
                <div class="burg3"></div>
            </a>
            <!-- Sidebar toggle -->

            <!-- Logo -->
            <a href="<?= base_url() ?>" class="brand-logo">
                <img src="<?= PATH_ADMIN ?>assets/_con/images/logo-white.png" alt="Con">
            </a>
            <!-- /Logo -->
        </div>
    </div>


    <div class="nano">
        <div class="nano-content">

            <ul>

                <li <?php
                if ($page_active == "dashboard") {
                    echo 'class="open"';
                }
                ?>>
                    <a href="<?= base_url() ?>fyadmin/dashboard" class="waves-effect waves-blue"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <?php
                if($this->session->userdata('Account_Type') == 1)
                {
                ?>
                <li <?php
                if ($page_active == "slider" || $page_active == "add_edit_slider") {
                    echo 'class="open"';
                }
                ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Slider<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li <?php
                        if ($page_active == "slider") {
                            echo 'class="active"';
                        }
                        ?>>
                            <a href="<?= base_url() ?>fyadmin/slider" class="waves-effect waves-blue"> View All</a>
                        </li>
                        <li <?php
                        if ($page_active == "add_edit_slider") {
                            echo 'class="active"';
                        }
                        ?>>
                            <a href="<?= base_url() ?>fyadmin/slider/add_edit_slider" class="waves-effect waves-blue"> Add New</a>
                        </li>
                    </ul>
                </li>

                <li <?php if ($page_active == "pages" || $page_active == "add_edit_pages") {
                                echo 'class="open"';
                            } ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Pages<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li <?php if ($page_active == "pages") {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?= base_url() ?>fyadmin/pages" class="waves-effect waves-blue"> View All</a>
                        </li>
                        <li <?php if ($page_active == "add_edit_pages") {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?= base_url() ?>fyadmin/pages/add_edit_pages" class="waves-effect waves-blue"> Add New</a>
                        </li>
                    </ul>
                </li> 

                <li <?php if ($page_active == "posts" || $page_active == "add_edit_posts	") {
                                echo 'class="open"';
                            } ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Posts<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li <?php if ($page_active == "posts") {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?= base_url() ?>fyadmin/posts" class="waves-effect waves-blue"> View All</a>
                        </li>
                        <li <?php if ($page_active == "add_edit_pages") {
                    echo 'class="active"';
                } ?>>
                            <a href="<?= base_url() ?>fyadmin/posts/add_edit_posts" class="waves-effect waves-blue"> Add New</a>
                        </li>
                    </ul>
                </li> 

                <li <?php
                if ($page_active == "view_category" || $page_active == "add_category" || $page_active == "edit_category" || $page_active == "activate_category" || $page_active == "delete_category") {
                    echo 'class="open"';
                }
                        ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Categories<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li <?php
                        if ($page_active == "view_category") {
                            echo 'class="active"';
                        }
                        ?>>
                            <a href="<?= base_url() ?>fyadmin/categories/view_category" class="waves-effect waves-blue"> View All</a>
                        </li>
                        <li <?php
                if ($page_active == "add_category") {
                    echo 'class="active"';
                }
                ?>>
                            <a href="<?= base_url() ?>fyadmin/categories/add_category" class="waves-effect waves-blue"> Add New</a>
                        </li>
                    </ul>
                </li>
                <?php
                }
                ?>
                <li <?php
                if ($page_active == "view_product" || $page_active == "add_product" || $page_active == "edit_product" || $page_active == "activate_product" || $page_active == "delete_product" || $page_active == "view_product_sold") {
                    echo 'class="open"';
                }
                ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Products<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li <?php
                        if ($page_active == "view_product") {
                            echo 'class="active"';
                        }
						?>>
                            <a href="<?= base_url() ?>fyadmin/products/view_product" class="waves-effect waves-blue"> View All</a>
                        </li>
                        <li <?php
                        if ($page_active == "add_product") {
                            echo 'class="active"';
                        }
						?>>
                            <a href="<?= base_url() ?>fyadmin/products/add_product" class="waves-effect waves-blue"> Add New</a>
                        </li>
					</ul>
                </li>
                
                <li <?php
                if ($page_active == "orders" || $page_active == "order_details") {
                    echo 'class="open"';
                }
                ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Orders<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li class="active">
                            <a href="<?= base_url() ?>fyadmin/accounts/orders" class="waves-effect waves-blue"> View All</a>
                        </li>
                    </ul>
                </li>
                <?php
                if($this->session->userdata('Account_Type') == 1)
                {
                ?>
                <li <?php if ($page_active == "photo_gallery" || $page_active == "add_edit_photo_gallery") {
                            echo 'class="open"';
                        } ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Photo Gallery<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li <?php if ($page_active == "photo_gallery") {
                    echo 'class="active"';
                } ?>>
                            <a href="<?= base_url() ?>fyadmin/photo_gallery" class="waves-effect waves-blue"> View All</a>
                        </li>
                        <li <?php if ($page_active == "add_edit_pages") {
                            echo 'class="active"';
                        } ?>>
                            <a href="<?= base_url() ?>fyadmin/photo_gallery/add_edit_photo_gallery" class="waves-effect waves-blue"> Add New</a>
                        </li>
                    </ul>
                </li> 
                
                <li <?php
                        if ($page_active == "view_account" || $page_active == "add_account" || $page_active == 'edit_account' || $page_active == 'delete_account' || $page_active == 'active_account') {
                            echo 'class="open"';
                        }
                ?>>
                    <a class="yay-sub-toggle waves-effect waves-blue"><i class="fa fa-table"></i> Manage Accounts<span class="yay-collapse-icon mdi-navigation-expand-more"></span></a>
                    <ul>
                        <li <?php
                        if ($page_active == "view_account") {
                            echo 'class="active"';
                        }
                ?>>
                            <a href="<?= base_url() ?>fyadmin/accounts/view_account" class="waves-effect waves-blue"> View All</a>
                        </li>
                        <li <?php
                        if ($page_active == "add_account") {
                            echo 'class="active"';
                        }
                ?>>
                            <a href="<?= base_url() ?>fyadmin/accounts/add_account" class="waves-effect waves-blue"> Add New</a>
                        </li>
                    </ul>
                </li>
                
                <?php
                }
                ?>
                
                <li <?php if ($page_active == "payment_setting") {
                            echo 'class="active"';
                        } ?>>
                    <a href="<?= base_url() ?>fyadmin/accounts/payment_setting" class="waves-effect waves-blue"><i class="fa fa-table"></i> Payment Setting</a>
                </li>    
                <?php
				if($this->session->userdata('Account_Type') == 1)
				{
				?>
				<li>
					<a href="<?=base_url()?>fyadmin/settings"><i class="fa fa-cogs"></i> Settings</a>
				</li>
				<?php
				}
				?>	
				<li>
					<a href="<?=base_url()?>fyadmin/home/logout"><i class="fa fa-sign-out"></i> Logout</a>
				</li>
            </ul>

        </div>
    </div>
</aside>