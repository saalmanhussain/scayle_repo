<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $site_Info['Title'] ?></title>

        <meta name="description" content="<?= $site_Info['MetaDescription'] ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

        <link rel="icon" type="image/png" href="<?= PATH_ADMIN ?>assets/_con/images/icon.png">

        <!-- nanoScroller -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/nanoScroller/nanoscroller.css" />


        <!-- FontAwesome -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/font-awesome/css/font-awesome.min.css" />

        <!-- Material Design Icons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/material-design-icons/css/material-design-icons.min.css" />

        <!-- IonIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/ionicons/css/ionicons.min.css" />

        <!-- WeatherIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/weatherIcons/css/weather-icons.min.css" />
        <!-- Main -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/_con/css/_con.min.css" />

        <!--[if lt IE 9]>
          <script src="<?= PATH_ADMIN ?>assets/html5shiv/html5shiv.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <section id="forgot-password">

            <!-- Background Bubbles -->
            <canvas id="bubble-canvas"></canvas>
            <!-- /Background Bubbles -->

            <!-- Reset Form -->
            <form action="" method="POST"  data-parsley-validate>
				<div class="col s12 logo center">
                    <img src="<?= PATH ?>upload/<?= $site_Info['Logo'] ?>" alt="">
                </div>

                <div class="card-panel">

                    <?php
                    if ($error != '') {
                        ?>
                        <div class="alert blue lighten-5 blue-text text-darken-2">
                            <strong><i class="fa fa-css3"></i></strong>&nbsp;We'll send instructions to reset your password to your email.
                        </div>
                        <?php
                    }
                    ?>



                    <div class="row">
                            <div class="input-field">
                                <i class="fa fa-envelope prefix"></i>
                                <input id="input_email" type="email" name="email" required="">
                                <label for="input_email">Email Address</label>
                            </div>
							
                            <button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover" name="forgot-btn">Reset</button>
                        
						<div class="col m12 s12">
                            <div class="right-align">
                                <a href="<?= base_url() ?>fyadmin/home">Remember your password?</a>
                            </div>
                        </div>
						
                    </div>

                </div>
            </form>
            <!-- /Reset Form -->

        </section>


        <!-- DEMO [REMOVE IT ON PRODUCTION] -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_demo.js"></script>

        <!-- jQuery -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery/jquery.min.js"></script>

        <!-- jQuery RAF (improved animation performance) -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

        <!-- nanoScroller -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

        <!-- Materialize -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/materialize/js/materialize.min.js"></script>

        <!-- Sortable -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/sortable/Sortable.min.js"></script>

        <!-- Main -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_con.min.js"></script>

        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/parsley/parsley.min.js"></script>

    </body>

</html>