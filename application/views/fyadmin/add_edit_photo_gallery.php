<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$site_Info['Title']?> | <?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit Photo Gallery";
			}else{
				echo "Add New Photo Gallery";
			}
			?></title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

  <link rel="icon" type="image/png" href="<?=PATH_ADMIN?>assets/_con/images/icon.png">

  <!-- nanoScroller -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/nanoScroller/nanoscroller.css" />


  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/font-awesome/css/font-awesome.min.css" />

  <!-- Material Design Icons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/material-design-icons/css/material-design-icons.min.css" />

  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/ionicons/css/ionicons.min.css" />

  <!-- WeatherIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/weatherIcons/css/weather-icons.min.css" />

  <!-- Google Prettify -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.css" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/_con/css/_con.min.css" />

  <!--[if lt IE 9]>
    <script src="<?=PATH_ADMIN?>assets/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?=PATH?>ckeditor/ckeditor.js"></script>
</head>

<body>


 
<!-- /Top Navbar -->
<?=$this->load->view('fyadmin/top-navbar');?>	
<!-- /Top Navbar -->

<!-- Sidebar -->
<?=$this->load->view('fyadmin/sidebar');?>	
<!-- /.sidebar -->
  


  <!-- Main Content -->
  <section class="content-wrap">


    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m9 l10">
			<h1>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit Photo Gallery ";
			}else{
				echo "Add New Photo Gallery ";
			}
			?>
			</h1>
          <ul>
            <li>
              <a href="<?=base_url()?>fyadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
            </li>
			<li>
              <a href="<?=base_url()?>fyadmin/pages"> View All</a>  <i class="fa fa-angle-right"></i>
            </li>
            <li><a>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit Photo Gallery";
			}else{
				echo "Add New Photo Gallery";
			}
			?>
			</a>
            </li>
          </ul>
        </div>
        <div class="col s12 m3 l2 right-align">
          <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
        </div>
      </div>

    </div>
    <!-- /Breadcrumb -->

		<?php
		if($this->session->userdata('Success')){
			echo '
			<div class="alert green lighten-4 green-text text-darken-2">
			'.$this->session->userdata('Success').'
			</div>
			<br>';
			$this->session->unset_userdata('Success');
		}
		
		if($this->session->userdata('Error')){
			echo '
				<div class="alert">
				  '.$this->session->userdata('Error').'
				</div>
		    </div>
			<br>';
			$this->session->unset_userdata('Error');
		}
		?>
<?php if($this->input->get('edit')=="yes"){?>
    <form action="do_edit_photo_gallery" method="post" enctype="multipart/form-data" data-parsley-validate>
      <div class="card-panel">
        <h4>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit";
			}else{
				echo "Add New";
			}
			?>	
		</h4>
	    <input type="hidden" name="photo_gallery_Id" id="photo_gallery_Id" value="<?=$photo_gallery[0]->photo_gallery_Id?>">
		
       <!-- Text Field -->
        <div class="row">
          <div class="col l12">
				<div class="input-field">
				  <input id="title" name="title" type="text" class="validate" required="" value="<?=$photo_gallery[0]->title?>">
				  <label for="title">Browser Title (Photo Album)</label>
				</div>
		   </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row" id="Image">
          <div class="col l2 l6">
            <div class="file-field input-field">
              <input class="file-path validate" type="text" />
              <div class="btn">
                <span>Images</span>
                <input type="file" name="Image[]" multiple="" />
              </div>
            </div>
          </div>
		  <?php foreach($gallery_details as $gallery_details){?>
		   <div class="col l2 s2">
             <div class="input-field">
				<a onClick="return confirmDelete();" href="add_edit_photo_gallery?i=<?=base64_encode($photo_gallery[0]->photo_gallery_Id)?>&edit=yes&delete=image&id=<?=base64_encode($gallery_details->gallery_images_Id)?>" class="btn red right">Remove this image?</a>
				<img src="<?=base_url().'assets/upload/'.$gallery_details->Image;?>" class="right" width="177" height="100">
            </div>
          </div>
		  <?php }?>
        </div>
        <!-- /Text Field -->
    
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
			  <textarea id="description" name="description" class="materialize-textarea"><?=$photo_gallery[0]->description?></textarea>
                <script type="text/javascript">
				CKEDITOR.replace( 'description',
				{
				filebrowserBrowseUrl : '<?=PATH?>ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl : '<?=PATH?>ckfinder/ckfinder.html?Type=Images',
				filebrowserFlashBrowseUrl : '<?=PATH?>ckfinder/ckfinder.html?Type=Flash',
				filebrowserUploadUrl : '<?=PATH?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl : '<?=PATH?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserFlashUploadUrl :'<?=PATH?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				});
				</script>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="1">Save</button>          
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="2">Save & Continue</button>          
			</div>
          </div>
        </div>
        <!-- /Text Field -->
		

		
      </div> 

    </form>
<?php }else{?>
	<form action="do_add_photo_gallery" method="post" enctype="multipart/form-data" data-parsley-validate>
      <div class="card-panel">
        <h4>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit";
			}else{
				echo "Add New";
			}
			?>	
		</h4>
		
       <!-- Text Field -->
        <div class="row">
          <div class="col l12">
				<div class="input-field">
				  <input id="title" name="title" type="text" class="validate" required="">
				  <label for="title">Browser Title (Photo Album)</label>
				</div>
		   </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row" id="Image">
          <div class="col l2 l12">
            <div class="file-field input-field">
              <input class="file-path validate" type="text" />
              <div class="btn">
                <span>Images</span>
                <input type="file" name="Image[]" multiple />
              </div>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
    
		<!-- Text Field -->
        <div class="row" id="Video">
          <div class="col l2 l12">
            <div class="input-field">
			  <textarea id="description" name="description" class="materialize-textarea"></textarea>
                <script type="text/javascript">
				CKEDITOR.replace( 'description',
				{
				filebrowserBrowseUrl : '<?=PATH?>ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl : '<?=PATH?>ckfinder/ckfinder.html?Type=Images',
				filebrowserFlashBrowseUrl : '<?=PATH?>ckfinder/ckfinder.html?Type=Flash',
				filebrowserUploadUrl : '<?=PATH?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl : '<?=PATH?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserFlashUploadUrl :'<?=PATH?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				});
				</script>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="1">Save</button>          
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="2">Save & Continue</button>          
			</div>
          </div>
        </div>
        <!-- /Text Field -->

      </div> 

    </form>
<?php }?>
  </section>
  <!-- /Main Content -->

		  <!-- Search Bar -->
		  <?=$this->load->view('fyadmin/search_bar');?>	
		  <!-- /Search Bar -->




		<!--Chat-->
		<?=$this->load->view('fyadmin/chat');?>	
		<!-- /Chat -->

  
		<!-- footer -->
		<?=$this->load->view('fyadmin/footer');?>	
		<!-- /.footer -->
  <!-- DEMO [REMOVE IT ON PRODUCTION] -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_demo.js"></script>

  <!-- jQuery -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery/jquery.min.js"></script>

  <!-- jQuery RAF (improved animation performance) -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

  <!-- nanoScroller -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

  <!-- Materialize -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/materialize/js/materialize.min.js"></script>

  <!-- Sortable -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/sortable/Sortable.min.js"></script>

  <!-- Main -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_con.min.js"></script>


  <!-- Google Prettify -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/parsley/parsley.min.js"></script>
</body>
<script>
function confirmDelete(){
	return confirm("Are you sure you want to delete this?");
}
</script>
</html>