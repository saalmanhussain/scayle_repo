<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$site_Info['Title']?></title>

  <meta name="description" content="<?=$site_Info['MetaDescription']?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

  <link rel="icon" type="image/png" href="<?=PATH_ADMIN?>assets/_con/images/icon.png">

  <!-- nanoScroller -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/nanoScroller/nanoscroller.css" />


  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/font-awesome/css/font-awesome.min.css" />

  <!-- Material Design Icons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/material-design-icons/css/material-design-icons.min.css" />

  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/ionicons/css/ionicons.min.css" />

  <!-- WeatherIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/weatherIcons/css/weather-icons.min.css" />

  <!-- Rickshaw -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/rickshaw/rickshaw.min.css" />

  <!-- jvectormap -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/jquery-jvectormap/jquery-jvectormap.css" />

  <!-- Google Prettify -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.css" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/_con/css/_con.min.css" />

  <!--[if lt IE 9]>
    <script src="<?=PATH_ADMIN?>assets/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
</head>

<body>

    <!-- /Top Navbar -->
    <?= $this->load->view('fyadmin/top-navbar'); ?>	
    <!-- /Top Navbar -->

    <!-- Sidebar -->
    <?= $this->load->view('fyadmin/sidebar'); ?>	
    <!-- /.sidebar -->


    <!-- Main Content -->
    <section class="content-wrap">


        <!-- Breadcrumb -->
        <div class="page-title">

            <div class="row">
                <div class="col s12 m9 l10">
                    <h1>Edit Account</h1>

                    <ul>
                        <li>
                            <a href="<?=base_url()?>fyadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                        </li>

                        <li>
                            <a href='<?=base_url()?>fyadmin/accounts/view_account'>Manage Accounts</a>  <i class='fa fa-angle-right'></i>
                        </li>
                        <li>
                            <a href='#'>Edit Account</a>
                        </li>

                    </ul>
                </div>
                <div class="col s12 m3 l2 right-align">
                    <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
                </div>
            </div>

        </div>
        <!-- /Breadcrumb -->

        
        <?php
		if($this->session->userdata('Success')){
			echo '
			<div class="alert green lighten-4 green-text text-darken-2">
			'.$this->session->userdata('Success').'
			</div>
			<br>';
			$this->session->unset_userdata('Success');
		}
		
		if($this->session->userdata('Error')){
			echo '
				<div class="alert">
				  '.$this->session->userdata('Error').'
				</div>
		    </div>
			<br>';
			$this->session->unset_userdata('Error');
		}
		?>



        <!-- With Export Options -->
        <div class="card-panel">
            
            <form method="POST" enctype="multipart/form-data" data-parsley-validate>

                <div class="input-field">
                    <i class="fa fa-user prefix"></i>
                    <input id="input_fname" type="text" name="name" value="<?= $user[0]->name ?>" required="">
                    <label for="input_fname">Name</label>
                </div>
               
                <div class="input-field">
                    <i class="fa fa-envelope prefix"></i>
                    <input id="input_email" type="email" name="email" value="<?= $user[0]->email ?>" required="">
                    <label for="input_email">Email</label>
                </div>

                <div class="input-field">
                    <i class="fa fa-unlock-alt prefix"></i>
                    <input id="input_password" type="password" name="password" value="" required="">
                    <label for="input_password">Password</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-map-marker prefix"></i>
                    <input id="input_country" type="text" name="country" value="<?= $user[0]->country ?>" required="">
                    <label for="input_country">Country</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-map-marker prefix"></i>
                    <input id="input_city" type="text" name="city" value="<?= $user[0]->city ?>" required="">
                    <label for="input_city">City</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-map-marker prefix"></i>
                    <input id="input_state" type="text" name="state" value="<?= $user[0]->state ?>" required="">
                    <label for="input_state">State</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-map-marker prefix"></i>
                    <input id="input_address" type="text" name="address" value="<?= $user[0]->address ?>" required="">
                    <label for="input_address">Address</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-phone prefix"></i>
                    <input id="input_phone" type="text" name="phone" value="<?= $user[0]->phone ?>" required="">
                    <label for="input_phone">Phone</label>
                </div>
                
                <select name="account_type" required="">
                    <option value="" disabled selected>Account Type</option>
                    <?php 
                    foreach($account_type as $row)
                    {
                    ?>
                    <option value="<?= $row->typeId ?>" <?php
                        if ($row->typeId == $user[0]->account_type) {
                            echo "selected";
                        }
                        ?>><?= $row->name ?></option>
                    <?php
                    }
                    ?>
                </select>

                <h4>Social Media Accounts</h4>
                
                <div class="input-field">
                    <i class="fa fa-facebook prefix"></i>
                    <input id="input_fb" type="text" value="<?= $user[0]->Facebook ?>" name="fb" >
                    <label for="input_fb">Facebook</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-twitter prefix"></i>
                    <input id="input_twitter" type="text" value="<?= $user[0]->Twitter ?>" name="twitter" >
                    <label for="input_twitter">Twitter</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-linkedin prefix"></i>
                    <input id="input_linkedin" type="text" value="<?= $user[0]->LinkedIn ?>" name="linkedin" >
                    <label for="input_linkedin">Linkedin</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-google-plus prefix"></i>
                    <input id="input_gplus" type="text" name="gplus" value="<?= $user[0]->Googleplus ?>" >
                    <label for="input_gplus">Google Plus</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-instagram prefix"></i>
                    <input id="input_instagram" type="text" name="instagram" value="<?= $user[0]->Instagram ?>" >
                    <label for="input_instagram">Instagram</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-youtube prefix"></i>
                    <input id="input_youtube" type="text" name="youtube" value="<?= $user[0]->YouTube ?>" >
                    <label for="input_youtube">Youtube</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-pinterest prefix"></i>
                    <input id="input_pinterest" type="text" name="pinterest" value="<?= $user[0]->Pinterest ?>" >
                    <label for="input_pinterest">Pinterest</label>
                </div>
                
                <div class="input-field">
                    <i class="fa fa-skype prefix"></i>
                    <input id="input_skype" type="text" name="skype" value="<?= $user[0]->SkypeID ?>" >
                    <label for="input_skype">Skype</label>
                </div>
                
                <div class="file-field input-field">
                    <img src="<?= PATH ?>upload/<?= $user[0]->profile_pic ?>" height='200' width='300' />
                    <input type="hidden" name="old_profile_image" value="<?= $user[0]->profile_pic ?>" />
                </div>
                
                <div class="file-field input-field">
                    <input class="file-path validate" type="text" name="profile_image" readonly="" />
                    <div class="btn">
                        <span>Profile</span>
                        <input type="file" id="profile_image" name="profile_image" accept=".jpg,.png,.JPG"  />
                    </div>
                </div>

                <input type="hidden" value="<?= $accountId ?>" name="accountID">
                
                <button name="add_account" class="waves-effect btn">Update</button>

            </form>
        </div>
        <!-- /With Export Options -->

    </section>
    <!-- /Main Content -->

    	<!--Chat-->
        <?= $this->load->view('fyadmin/search_bar'); ?>	
        <!-- /Chat -->

        <!--Chat-->
        <?= $this->load->view('fyadmin/chat'); ?>	
        <!-- /Chat -->
    <!-- /Chat -->

    <!-- footer -->
    <?= $this->load->view('fyadmin/footer'); ?>	
    <!-- /.footer -->
    
    <!-- DEMO [REMOVE IT ON PRODUCTION] -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_demo.js"></script>

  <!-- jQuery -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery/jquery.min.js"></script>

  <!-- jQuery RAF (improved animation performance) -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

  <!-- nanoScroller -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

  <!-- Materialize -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/materialize/js/materialize.min.js"></script>


  <!-- Simple Weather -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/simpleWeather/jquery.simpleWeather.min.js"></script>

  <!-- Sparkline -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/sparkline/jquery.sparkline.min.js"></script>

  <!-- Flot -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/flot/jquery.flot.min.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/flot/jquery.flot.time.min.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/flot/jquery.flot.pie.min.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/flot/jquery.flot.tooltip.min.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/flot/jquery.flot.categories.min.js"></script>

  <!-- d3 -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/d3/d3.min.js"></script>

  <!-- Rickshaw -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/rickshaw/rickshaw.min.js"></script>

  <!-- jvectormap -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery-jvectormap/jquery-jvectormap.min.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery-jvectormap/gdp-data.js"></script>
  <!-- Sortable -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/sortable/Sortable.min.js"></script>

  <!-- Main -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_con.min.js"></script>


  <!-- Google Prettify -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.js"></script>
  
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/parsley/parsley.min.js"></script>

    
</body>
</html>