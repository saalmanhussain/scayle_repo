<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $site_Info['Title'] ?></title>

        <meta name="description" content="<?= $site_Info['MetaDescription'] ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

        <link rel="icon" type="image/png" href="<?= PATH_ADMIN ?>assets/_con/images/icon.png">

        <!-- nanoScroller -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/nanoScroller/nanoscroller.css" />


        <!-- FontAwesome -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/font-awesome/css/font-awesome.min.css" />

        <!-- Material Design Icons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/material-design-icons/css/material-design-icons.min.css" />

        <!-- IonIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/ionicons/css/ionicons.min.css" />

        <!-- WeatherIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/weatherIcons/css/weather-icons.min.css" />

        <!-- Rickshaw -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/rickshaw/rickshaw.min.css" />

        <!-- jvectormap -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/jquery-jvectormap/jquery-jvectormap.css" />

        <!-- Google Prettify -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/google-code-prettify/prettify.css" />
        <!-- Main -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/_con/css/_con.min.css" />

        <!--[if lt IE 9]>
          <script src="<?= PATH_ADMIN ?>assets/html5shiv/html5shiv.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <!-- /Top Navbar -->
        <?= $this->load->view('fyadmin/top-navbar'); ?>	
        <!-- /Top Navbar -->


        <!-- Sidebar -->
        <?= $this->load->view('fyadmin/sidebar'); ?>	
        <!-- /.sidebar -->


        <!-- Main Content -->
        <section class="content-wrap">


    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m9 l10">
          <h1>Dashboard</h1>

          <ul>
            <li>
              <a href="#"><i class="fa fa-home"></i> Home</a>
            </li>
          </ul>
        </div>
        <div class="col s12 m3 l2 right-align">
          <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
        </div>
      </div>

    </div>
    <!-- /Breadcrumb -->

    <!-- Stats Panels -->
    <div class="row sortable">
        <div class="col l6 m6 s12">
            <a href="#" class="card-panel stats-card red lighten-2 red-text text-lighten-5">
                <i class="fa fa-users"></i>
                <span class="count"><?= $this->menu->total_users(); ?></span>
                <div class="name">Total Users</div>
            </a>
        </div>
        <div class="col l6 m6 s12">
            <a href="#" class="card-panel stats-card blue lighten-2 blue-text text-lighten-5">
                <i class="fa fa-users"></i>
                <span class="count"><?= $this->menu->total_dealers(); ?></span>
                <div class="name">Total Dealers</div>
            </a>
        </div>


        <div class="col l4 m6 s12">
            <a href="#" class="card-panel stats-card red lighten-2 red-text text-lighten-5">
                <i class="fa fa-users"></i>
                <span class="count"><?= $this->menu->total_users_day(); ?></span>
                <div class="name">Day</div>
            </a>
        </div>

        <div class="col l4 m6 s12">
            <a href="#" class="card-panel stats-card teal lighten-2 amber-text text-lighten-5">
                <i class="fa fa-users"></i>
                <span class="count"><?= $this->menu->total_users_week(); ?></span>
                <div class="name">Week</div>
            </a>
        </div>


        <div class="col l4 m6 s12">
            <a href="#" class="card-panel stats-card blue lighten-2 blue-text text-lighten-5">
                <i class="fa fa-users"></i>
                <span class="count"><?= $this->menu->total_users_month(); ?></span>
                <div class="name">Month</div>
            </a>
        </div>
    </div>
	<audio id="myaudio" src="../assets/notify.mp3"></audio>
	<!-- /Stats Panels -->
	<span id="screen"></span>
  </section>
        <!-- /Main Content -->


        <!--Chat-->
        <?= $this->load->view('fyadmin/search_bar'); ?>	
        <!-- /Chat -->

        <!--Chat-->
        <?= $this->load->view('fyadmin/chat'); ?>	
        <!-- /Chat -->

        <!-- footer -->
        <?= $this->load->view('fyadmin/footer'); ?>	
        <!-- /.footer -->

        <!-- DEMO [REMOVE IT ON PRODUCTION] -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_demo.js"></script>

        <!-- jQuery -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery/jquery.min.js"></script>

        <!-- jQuery RAF (improved animation performance) -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

        <!-- nanoScroller -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

        <!-- Materialize -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/materialize/js/materialize.min.js"></script>


        <!-- Simple Weather -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/simpleWeather/jquery.simpleWeather.min.js"></script>

        <!-- Sparkline -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/sparkline/jquery.sparkline.min.js"></script>

        <!-- Flot -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.time.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.pie.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.tooltip.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.categories.min.js"></script>

        <!-- d3 -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/d3/d3.min.js"></script>

        <!-- Rickshaw -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/rickshaw/rickshaw.min.js"></script>

        <!-- jvectormap -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery-jvectormap/jquery-jvectormap.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery-jvectormap/gdp-data.js"></script>
        <!-- Sortable -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/sortable/Sortable.min.js"></script>

        <!-- Main -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_con.min.js"></script>


        <!-- Google Prettify -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/google-code-prettify/prettify.js"></script>
        <script>
		$(document).ready(function(){
			setInterval(function(){
			$("#screen").load('dashboard/orders');
			}, 2000);
		});
		</script>
		<script>
            /*
             * Sparkline in Card panel
             */
            (function () {
                $("#sparkcard1").conSparkline([76, 78, 87, 65, 43, 35, 23, 25, 12, 14, 27, 35, 32, 37, 31, 46, 43, 32, 36, 57, 78, 87, 82, 75, 58, 54, 70, 23, 54, 67, 34, 23, 87, 12, 43, 65, 23, 76, 32, 55], {
                    type: 'bar',
                    width: '100%',
                    height: 20,
                    barColor: '#2196f3'
                });
            }());


            var rickshawLine1 = [{"x": 0, "y": 13}, {"x": 1, "y": 12}, {"x": 2, "y": 24}, {"x": 3, "y": 25}, {"x": 4, "y": 12}, {"x": 5, "y": 16}, {"x": 6, "y": 24}, {"x": 7, "y": 13}, {"x": 8, "y": 12}, {"x": 9, "y": 11}];
            var rickshawLine2 = [{"x": 0, "y": 16}, {"x": 1, "y": 23}, {"x": 2, "y": 17}, {"x": 3, "y": 16}, {"x": 4, "y": 22}, {"x": 5, "y": 25}, {"x": 6, "y": 21}, {"x": 7, "y": 22}, {"x": 8, "y": 12}, {"x": 9, "y": 13}];

            /*
             * Rickshaw Stacked Area Chart
             */
            (function () {
                var element = $("#rickshawStackedArea");
                var graph = new Rickshaw.Graph({
                    element: element[0],
                    renderer: 'area',
                    stroke: false,
                    series: [{
                            data: rickshawLine1,
                            color: '#42a5f5',
                            name: 'Visits'
                        }, {
                            data: rickshawLine2,
                            color: '#90caf9',
                            name: 'Views'
                        }]
                });
                graph.render();

                var hoverDetail = new Rickshaw.Graph.HoverDetail({
                    graph: graph,
                    xFormatter: function (x) {
                        return x
                    },
                    yFormatter: function (y) {
                        return y
                    }
                });

                // responsive
                $(window).on('resize', function () {
                    graph.configure({
                        width: element.width()
                    });
                    graph.render();
                });
            }());


            /*
             * Rickshaw Stacked Bars
             */
            (function () {
                var element = $("#rickshawStackedBars");
                var graph = new Rickshaw.Graph({
                    element: element[0],
                    renderer: 'bar',
                    series: [{
                            data: rickshawLine1,
                            color: '#26a69a',
                            name: 'Visits'
                        }, {
                            data: rickshawLine2,
                            color: '#80cbc4',
                            name: 'Views'
                        }]
                });
                graph.render();

                var hoverDetail = new Rickshaw.Graph.HoverDetail({
                    graph: graph,
                    xFormatter: function (x) {
                        return x
                    },
                    yFormatter: function (y) {
                        return y
                    }
                });

                // responsive
                $(window).on('resize', function () {
                    graph.configure({
                        width: element.width()
                    });
                    graph.render();
                });
            }());



            /*
             * Flot Line Chart
             */
            (function () {
                var chart = $("#flotLineChart");
                var data1 = {
                    data: [[1, 50], [2, 58], [3, 45], [4, 62], [5, 55], [6, 65], [7, 61], [8, 70], [9, 65], [10, 70], [11, 53], [12, 49]],
                    label: "Mails"
                };
                var data2 = {
                    data: [[1, 25], [2, 31], [3, 23], [4, 48], [5, 38], [6, 40], [7, 47], [8, 55], [9, 43], [10, 50], [11, 37], [12, 29]],
                    label: "SMS"
                };
                var data3 = {
                    data: [[1, 4], [2, 13], [3, 7], [4, 17], [5, 20], [6, 24], [7, 13], [8, 17], [9, 10], [10, 17], [11, 6], [12, 3]],
                    label: "Invoices"
                };
                var options = {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 1,
                            fill: true,
                            fillColor: {colors: [{opacity: 0.1}, {opacity: 0.13}]}
                        },
                        points: {
                            show: true,
                            lineWidth: 2,
                            radius: 3
                        },
                        shadowSize: 0,
                        stack: true
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#f9f9f9",
                        borderWidth: 0
                    },
                    legend: {
                        // show: false
                        backgroundOpacity: 0,
                        labelBoxBorderColor: "#fff"
                    },
                    colors: ["#3f51b5", "#009688", "#2196f3"],
                    xaxis: {
                        ticks: [[1, "Jan"], [2, "Feb"], [3, "Mar"], [4, "Apr"], [5, "May"], [6, "Jun"],
                            [7, "Jul"], [8, "Aug"], [9, "Sep"], [10, "Oct"], [11, "Nov"], [12, "Dec"]],
                        font: {
                            family: "Roboto,sans-serif",
                            color: "#ccc"
                        }
                    },
                    yaxis: {
                        ticks: 7,
                        tickDecimals: 0,
                        font: {color: "#ccc"}
                    }
                };

                function initFlot() {
                    $.plot(chart, [data1, data2, data3], options);
                    chart.find('.legend table').css('width', 'auto')
                            .find('td').css('padding', 5);
                }
                initFlot();
                $(window).on('resize', initFlot);

                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y - 40,
                        left: x - 55,
                        color: "#fff",
                        padding: '5px 10px',
                        'border-radius': '3px',
                        'background-color': 'rgba(0,0,0,0.6)'
                    }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
                chart.bind("plothover", function (event, pos, item) {
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(0),
                                    y = item.datapoint[1].toFixed(0);

                            var month = item.series.xaxis.ticks[item.dataIndex].label;

                            showTooltip(item.pageX, item.pageY,
                                    item.series.label + " of " + month + ": " + y);
                        }
                    }
                    else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }());



            /*
             * Flot Pie Chart
             */
            (function () {
                var chart = $("#flotPieChart");
                var data = [
                    {label: "IE", data: 19.5, color: "#90a4ae"},
                    {label: "Safari", data: 4.5, color: "#7986cb"},
                    {label: "Opera", data: 2.3, color: "#9575cd"},
                    {label: "Firefox", data: 36.6, color: "#4db6ac"},
                    {label: "Chrome", data: 36.3, color: "#64b5f6"}
                ];
                var options = {
                    series: {
                        pie: {
                            innerRadius: 0.5,
                            show: true
                        }
                    },
                    grid: {
                        hoverable: true
                    },
                    legend: {
                        backgroundOpacity: 0,
                        labelBoxBorderColor: "#fff"
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    }
                };

                function initFlot() {
                    $.plot(chart, data, options);
                    chart.find('.legend table').css('width', 'auto')
                            .find('td').css('padding', 5);
                }
                initFlot();
                $(window).on('resize', initFlot);

            }());


            /*
             * MAP 1
             */
            (function () {
                $('#map1').vectorMap({
                    map: 'world_mill_en',
                    zoom: 2,
                    series: {
                        regions: [{
                                values: gdpData,
                                scale: ['#e3f2fd', '#2196f3'],
                                normalizeFunction: 'polynomial'
                            }]
                    },
                    backgroundColor: '#fff',
                    onRegionTipShow: function (e, el, code) {
                        el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                    }
                });
            }());




            /*
             * Rickshaw
             */
            (function () {
                var rickshawSeries = [[], []];

                // Create random data
                var randomData = new Rickshaw.Fixtures.RandomData(50);
                for (var i = 0; i < 40; i++) {
                    randomData.addData(rickshawSeries);
                }

                // Init Richshaw graph
                var element = $("#rickshawGraph");
                var graph = new Rickshaw.Graph({
                    element: element[0],
                    height: 253,
                    interpolation: 'cardinal',
                    renderer: 'area',
                    series: [
                        {
                            data: rickshawSeries[0],
                            color: '#4db6ac',
                            name: 'HDD'
                        }, {
                            data: rickshawSeries[1],
                            color: '#b2dfdb',
                            name: 'CPU'
                        }
                    ]
                });

                // Add hover info
                new Rickshaw.Graph.HoverDetail({
                    graph: graph
                });

                // Render graph
                graph.render();

                // Live Update
                setInterval(function () {
                    randomData.removeData(rickshawSeries);
                    randomData.addData(rickshawSeries);
                    graph.update();
                }, 1000);

                // Responsive
                $(window).on('resize', function () {
                    graph.configure({
                        width: element.width()
                    });
                    graph.render();
                });

            }());


            setTimeout(function () {
                Materialize.toast('<?php
        // I'm Pakistan so my timezone is Asia/Karachi
        date_default_timezone_set('Asia/Karachi');
        // 24-hour format of an hour without leading zeros (0 through 23)
        $Hour = date('G');
        if ($Hour >= 5 && $Hour <= 11) {
            echo "Good Morning, ";
        } else if ($Hour >= 12 && $Hour <= 18) {
            echo "Good Afternoon, ";
        } else if ($Hour >= 19 && $Hour <= 23) {
            echo "Good Evening, ";
        } else {
            echo "Good Night, ";
        }
        echo ucwords($this->session->userdata('name')) . "!";
        ?>', 1000);
            }, 1000);


            // setTimeout(function() {
            //   $(window).resize();
            // }, 1);
        </script>
    </body>
</html>