<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$site_Info['Title']?> | View All Photo Gallery</title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

  <link rel="icon" type="image/png" href="<?=PATH_ADMIN?>assets/_con/images/icon.png">

  <!-- nanoScroller -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/nanoScroller/nanoscroller.css" />


  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/font-awesome/css/font-awesome.min.css" />

  <!-- Material Design Icons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/material-design-icons/css/material-design-icons.min.css" />

  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/ionicons/css/ionicons.min.css" />

  <!-- WeatherIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/weatherIcons/css/weather-icons.min.css" />

  <!-- Google Prettify -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.css" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/_con/css/_con.min.css" />

  <!--[if lt IE 9]>
    <script src="<?=PATH_ADMIN?>assets/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
</head>

<body>


<!-- /Top Navbar -->
<?=$this->load->view('fyadmin/top-navbar');?>	
<!-- /Top Navbar -->

<!-- Sidebar -->
<?=$this->load->view('fyadmin/sidebar');?>	
<!-- /.sidebar -->


  <!-- Main Content -->
  <section class="content-wrap">


    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m9 l10">
          <h1>View Photo Gallery</h1>

          <ul>
            <li>
              <a href="<?=base_url()?>fyadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
            </li>
			<li>
              <a> View All</a>
            </li>
          </ul>
        </div>
        <div class="col s12 m3 l2 right-align">
          <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
        </div>
      </div>

    </div>
    <!-- /Breadcrumb -->

  
		<?php
		if($this->session->userdata('Success')){
			echo '
			<div class="alert green lighten-4 green-text text-darken-2">
			'.$this->session->userdata('Success').'
			</div>
			<br>';
			$this->session->unset_userdata('Success');
		}
		
		if($this->session->userdata('Error')){
			echo '
				<div class="alert">
				  '.$this->session->userdata('Error').'
				</div>
		    </div>
			<br>';
			$this->session->unset_userdata('Error');
		}
		?>
    

    <!-- With Export Options -->
    <div class="card-panel">
      <h4>Posts</h4>
      <div class="row">
        <div class="col s12 12">
          <table id="table4" class="display table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>created</th>
                <th>Status</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    <!-- /With Export Options -->

  </section>
  <!-- /Main Content -->

		  <!-- Search Bar -->
		  <?=$this->load->view('fyadmin/search_bar');?>	
		  <!-- /Search Bar -->

		<!--Chat-->
		<?=$this->load->view('fyadmin/chat');?>	
		<!-- /Chat -->

		<!-- footer -->
		<?=$this->load->view('fyadmin/footer');?>	
		<!-- /.footer -->
		
  <!-- DEMO [REMOVE IT ON PRODUCTION] -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_demo.js"></script>

  <!-- jQuery -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery/jquery.min.js"></script>

  <!-- jQuery RAF (improved animation performance) -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

  <!-- nanoScroller -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

  <!-- Materialize -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/materialize/js/materialize.min.js"></script>


  <!-- Data Tables -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/dataTables/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/dataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/dataTables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
  <!-- Sortable -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/sortable/Sortable.min.js"></script>

  <!-- Main -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_con.min.js"></script>


  <!-- Google Prettify -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.js"></script>
  <script>
	function confirmDelete(){
		return confirm("Are you sure you want to delete this?");
	}
    /*
     * DataTable with Export Options
     */
    $('#table4').DataTable({
	  "ajax": "<?= base_url()?>fyadmin/photo_gallery/photo_gallery_Json",
      "deferRender": true,
      "iDisplayLength": 25,
      "aLengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "all"]
      ],
      "bLengthChange": true,
      "filter": true,
      "dom": 'Tlfrtip',
      "tableTools": {
        "sSwfPath": "<?=PATH_ADMIN?>assets/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });
  </script>
</body>
</html>