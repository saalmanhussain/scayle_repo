<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$site_Info['Title']?></title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

  <link rel="icon" type="image/png" href="<?=PATH_ADMIN?>assets/_con/images/icon.png">

  <!-- nanoScroller -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/nanoScroller/nanoscroller.css" />


  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/font-awesome/css/font-awesome.min.css" />

  <!-- Material Design Icons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/material-design-icons/css/material-design-icons.min.css" />

  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/ionicons/css/ionicons.min.css" />

  <!-- WeatherIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/weatherIcons/css/weather-icons.min.css" />

  <!-- Google Prettify -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.css" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/_con/css/_con.min.css" />

  <!--[if lt IE 9]>
    <script src="<?=PATH_ADMIN?>assets/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
</head>

<body>


 
<!-- /Top Navbar -->
<?=$this->load->view('fyadmin/top-navbar');?>	
<!-- /Top Navbar -->

<!-- Sidebar -->
<?=$this->load->view('fyadmin/sidebar');?>	
<!-- /.sidebar -->
  


  <!-- Main Content -->
  <section class="content-wrap">


    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m9 l10">
			<h1>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit Slider";
			}else{
				echo "Add New Slider";
			}
			?>
			</h1>
          <ul>
            <li>
              <a href="<?=base_url()?>fyadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
            </li>
			<li>
              <a href="<?=base_url()?>fyadmin/slider"> View All</a>  <i class="fa fa-angle-right"></i>
            </li>
            <li><a>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit Slider";
			}else{
				echo "Add New Slider";
			}
			?>
			</a>
            </li>
          </ul>
        </div>
        <div class="col s12 m3 l2 right-align">
          <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
        </div>
      </div>

    </div>
    <!-- /Breadcrumb -->

		<?php
		if($this->session->userdata('Success')){
			echo '
			<div class="alert green lighten-4 green-text text-darken-2">
			'.$this->session->userdata('Success').'
			</div>
			<br>';
			$this->session->unset_userdata('Success');
		}
		
		if($this->session->userdata('Error')){
			echo '
				<div class="alert">
				  '.$this->session->userdata('Error').'
				</div>
		    </div>
			<br>';
			$this->session->unset_userdata('Error');
		}
		?>
<?php if($this->input->get('edit')=="yes"){?>
    <form action="do_edit_slider" method="post" enctype="multipart/form-data" data-parsley-validate>
	<input type="hidden" name="sliderId" id="sliderId" value="<?=$slider[0]->sliderId?>">
      <div class="card-panel">
        <h4>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit";
			}else{
				echo "Add New";
			}
			?>	
		</h4>

        <!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <select  name="type" id="type" class="validate" required="">
              <option value="" disabled selected>Select Slider Type</option>
              <option value="1" <?php if($slider[0]->type =="1"){echo "selected";}?>>Image</option>
              <option value="2" <?php if($slider[0]->type =="2"){echo "selected";}?>>Video</option>
            </select>
          </div>
        </div>
        <!-- /Text Field -->
		
       
		<!-- Text Field -->
        <div class="row" id="Image">
          <div class="col l2 l<?php if($slider[0]->slider){echo "6";}else{echo "12";}?>">
            <div class="file-field input-field">
              <input class="file-path validate" type="text" />
              <div class="btn">
                <span>Upload Image</span>
                <input type="file" name="slider" />
              </div>
            </div>
          </div>
		  <div class="col l2 l6">
            <div class="file-field input-field">
			<?php if($slider[0]->slider){?>
			 <a onClick="return confirmDelete();" href="add_edit_slider?i=<?=base64_encode($slider[0]->sliderId)?>&edit=yes&delete=image&id=<?=base64_encode($slider[0]->sliderId)?>" class="btn red right">Remove this image?</a>
             <a href="<?=base_url().'assets/upload/'.$slider[0]->slider;?>" target="_blank">
				<img src="<?=base_url().'assets/upload/'.$slider[0]->slider;?>" class="right" width="200">
             </a>
			<?php }?>
			</div>
          </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row" id="Video">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="slider" name="slider" type="text" class="validate" value="<?=$slider[0]->slider;?>">
              <label for="slider">Embed Code</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col s2">
            <div class="input-field">
              <label for="Set_Text">Set Text:</label>
            </div>
          </div>
		    <span class="dynamicInput">
			<?php $count = 0; foreach($slider_details as $slider_details){ $count++;?>
				<?php if($count!=1){?>
				<div class="col s2">
					<a onClick="return confirmDelete();" href="add_edit_slider?i=<?=base64_encode($slider[0]->sliderId)?>&edit=yes&delete=text&id=<?=base64_encode($slider_details->detailId)?>" class="waves-effect waves-dark btn red">Remove this row?</a>
				</div>	
				<?php }?>
			  <div class="col s2">
				<div class="input-field">
				  <input id="text" name="text[]" type="text" value="<?=$slider_details->text?>" class="validate">
				  <label for="text">Text Here 1</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="color" name="color[]" type="text" value="<?=$slider_details->color?>" class="validate">
				  <label for="color">Text Here 2</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="size" name="size[]" type="text" value="<?=$slider_details->size?>" class="validate">
				  <label for="size">Text Here 3</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="top" name="top[]" type="text" value="<?=$slider_details->top?>" class="validate">
				  <label for="top">Text Here 4</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="left" name="left[]" type="text" value="<?=$slider_details->left?>" class="validate">
				  <label for="left">Text Here 5</label>
				</div>
			  </div>
			  <?php }?>
		    </span>	  
		</div>
		
        <!-- /Text Field -->
		<span class="waves-effect waves-dark btn purple right addInput">+add more</span>
		
		<!-- Text Field -->
        <div class="row" id="Video">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="orderBy" name="orderBy" type="text" class="validate" required="" value="<?=$slider[0]->orderBy;?>">
              <label for="orderBy">OrderBy</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="1">Save</button>          
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="2">Save & Continue</button>          
			</div>
          </div>
        </div>
        <!-- /Text Field -->
		

		
      </div> 

    </form>
<?php }else{?>
	<form action="do_add_slider" method="post" enctype="multipart/form-data" data-parsley-validate>
      <div class="card-panel">
        <h4>
			<?php 
			if($this->input->get('edit')=="yes"){
				echo "Edit";
			}else{
				echo "Add New";
			}
			?>	
		</h4>

        <!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <select  name="type" id="type" class="validate" required="">
              <option value="" disabled selected>Select Slider Type</option>
              <option value="1">Image</option>
              <option value="2">Video</option>
            </select>
          </div>
        </div>
        <!-- /Text Field -->
		
       
		<!-- Text Field -->
        <div class="row" id="Image">
          <div class="col l2 l12">
            <div class="file-field input-field">
              <input class="file-path validate" type="text" />
              <div class="btn">
                <span>Upload Image</span>
                <input type="file" name="slider" />
              </div>
            </div>
          </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row" id="Video">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="slider" name="slider" type="text" class="validate">
              <label for="slider">Embed Code</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col s2">
            <div class="input-field">
              <label for="Set_Text">Set Text:</label>
            </div>
          </div>
		    <span class="dynamicInput">
			  <div class="col s2">
				<div class="input-field">
				  <input id="text" name="text[]" type="text" class="validate">
				  <label for="text">Text Here 1</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="color" name="color[]" type="text" class="validate">
				  <label for="color">Text Here 2</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="size" name="size[]" type="text" class="validate">
				  <label for="size">Text Here 3</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="top" name="top[]" type="text" class="validate">
				  <label for="top">Text Here 4</label>
				</div>
			  </div>
			  <div class="col s2">
				<div class="input-field">
				  <input id="left" name="left[]" type="text" class="validate">
				  <label for="left">Text Here 5</label>
				</div>
			  </div>
		    </span>	  
		</div>
		
        <!-- /Text Field -->
		<span class="waves-effect waves-dark btn purple right addInput">+add more</span>
		
		<!-- Text Field -->
        <div class="row" id="Video">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="orderBy" name="orderBy" type="text" class="validate" required="">
              <label for="orderBy">OrderBy</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="1">Save</button>          
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover btn_Submit"  name="btn" value="2">Save & Continue</button>          
			</div>
          </div>
        </div>
        <!-- /Text Field -->
		

		
      </div> 

    </form>
<?php }?>
  </section>
  <!-- /Main Content -->

		  <!-- Search Bar -->
		  <?=$this->load->view('fyadmin/search_bar');?>	
		  <!-- /Search Bar -->




		<!--Chat-->
		<?=$this->load->view('fyadmin/chat');?>	
		<!-- /Chat -->

  
		<!-- footer -->
		<?=$this->load->view('fyadmin/footer');?>	
		<!-- /.footer -->
  <!-- jQuery -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery/jquery.min.js"></script>

  <!-- jQuery RAF (improved animation performance) -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

  <!-- nanoScroller -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

  <!-- Materialize -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/materialize/js/materialize.min.js"></script>

  <!-- Sortable -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/sortable/Sortable.min.js"></script>

  <!-- Main -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_con.min.js"></script>

  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/parsley/parsley.min.js"></script>

</body>
<script>
	function confirmDelete(){
		return confirm("Are you sure you want to delete this?");
	}
	var type = $("#type").val();
   if(type == "1"){
	   $("#Video").hide();
	   $("#Image").show(); 
   }else if(type == "2"){
	   $("#Image").hide();
	   $("#Video").show(); 
   }else{
	   $("#Image").hide();
	   $("#Video").hide();
   }
$("#type").change(function() {
	var val =  $(this).val();
   if(val=="1"){
	   $("#Video").hide();
	   $("#Image").show(); 
   }else if(val=="2"){
	   $("#Image").hide();
	   $("#Video").show(); 
   }else{
	   $("#Image").hide();
	   $("#Video").hide();
   }
});


$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".dynamicInput"); //Fields wrapper
    var add_button      = $(".addInput"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col s12"><a href="#" class="remove_field waves-effect waves-dark btn red left">Remove this row?</a>&nbsp;<div class="col s2">'+
				'<div class="input-field">'+
				  '<input id="text" name="text[]" type="text" class="validate">'+
				  '<label for="text'+ x +'">Text Here 1</label>'+
				'</div>'+
			  '</div>'+
			  '<div class="col s2">'+
				'<div class="input-field">'+
				  '<input id="color" name="color[]" type="text" class="validate">'+
				  '<label for="color'+ x +'">Text Here 2</label>'+
				'</div>'+
			  '</div>'+
			  '<div class="col s2">'+
				'<div class="input-field">'+
				  '<input id="size" name="size[]" type="text" class="validate">'+
				  '<label for="size'+ x +'">Text Here 3</label>'+
				'</div>'+
			  '</div>'+
			  '<div class="col s2">'+
				'<div class="input-field">'+
				  '<input id="top" name="top[]" type="text" class="validate">'+
				  '<label for="top'+ x +'">Text Here 4</label>'+
				'</div>'+
			  '</div>'+
			  '<div class="col s2">'+
				'<div class="input-field">'+
				  '<input id="left" name="left[]" type="text" class="validate">'+
				  '<label for="left'+ x +'">Text Here 5</label>'+
				'</div>'+
			  '</div></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });	
});
</script>
</html>