<nav class="navbar-top">
    <div class="nav-wrapper">

      <!-- Sidebar toggle -->
      <a href="#" class="yay-toggle">
        <div class="burg1"></div>
        <div class="burg2"></div>
        <div class="burg3"></div>
      </a>
      <!-- Sidebar toggle -->

      <!-- Logo -->
      <a href="<?=base_url()?>fyadmin/dashboard" class="brand-logo">
        <img src="<?=PATH?>upload/<?=$site_Info['Logo']?>" alt="Con">
      </a>
      <!-- /Logo -->

      <!-- Menu -->
      <ul>
        <!--<li><a href="#!" class="search-bar-toggle"><i class="mdi-action-search"></i></a>
        </li>-->
        <li class="user">
          <a class="dropdown-button" href="#!" data-activates="user-dropdown">
            <img src="<?=PATH?>upload/<?=$profile_Info['Profile_Pic']?>" alt="John Doe" class="circle"><?=ucwords($this->session->userdata('name'))?><i class="mdi-navigation-expand-more right"></i>
          </a>

          <ul id="user-dropdown" class="dropdown-content">
            <li><a href="<?= base_url() ?>fyadmin/accounts/edit_profile"><i class="fa fa-user"></i> Profile</a>
            </li>
            <?php
            if($this->session->userdata('Account_Type') == 1)
            {
            ?>
            <li><a href="<?=base_url()?>fyadmin/settings"><i class="fa fa-cogs"></i> Settings</a>
            </li>
            <?php
            }
            ?>
            <li class="divider"></li>
            <li><a href="<?=base_url()?>fyadmin/home/logout"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
          </ul>
        </li>
      </ul>
      <!-- /Menu -->

    </div>
  </nav>