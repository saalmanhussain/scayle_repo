<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$site_Info['Title']?></title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

  <link rel="icon" type="image/png" href="<?=PATH_ADMIN?>assets/_con/images/icon.png">

  <!-- nanoScroller -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/nanoScroller/nanoscroller.css" />


  <!-- FontAwesome -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/font-awesome/css/font-awesome.min.css" />

  <!-- Material Design Icons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/material-design-icons/css/material-design-icons.min.css" />

  <!-- IonIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/ionicons/css/ionicons.min.css" />

  <!-- WeatherIcons -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/weatherIcons/css/weather-icons.min.css" />

  <!-- Google Prettify -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.css" />
  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN?>assets/_con/css/_con.min.css" />

  <!--[if lt IE 9]>
    <script src="<?=PATH_ADMIN?>assets/html5shiv/html5shiv.min.js"></script>
  <![endif]-->
</head>

<body>


 
<!-- /Top Navbar -->
<?=$this->load->view('fyadmin/top-navbar');?>	
<!-- /Top Navbar -->

<!-- Sidebar -->
<?=$this->load->view('fyadmin/sidebar');?>	
<!-- /.sidebar -->
  


  <!-- Main Content -->
  <section class="content-wrap">


    <!-- Breadcrumb -->
    <div class="page-title">

      <div class="row">
        <div class="col s12 m9 l10">
          <h1>Site Settings</h1>

          <ul>
            <li>
              <a href="<?=base_url()?>fyadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
            </li>
            <li><a>Site Settings</a>
            </li>
          </ul>
        </div>
        <div class="col s12 m3 l2 right-align">
          <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
        </div>
      </div>

    </div>
    <!-- /Breadcrumb -->

		<?php
		if($this->session->userdata('Success')){
			echo '
			<div class="alert green lighten-4 green-text text-darken-2">
			'.$this->session->userdata('Success').'
			</div>
			<br>';
			$this->session->unset_userdata('Success');
		}
		
		if($this->session->userdata('Error')){
			echo '
				<div class="alert">
				  '.$this->session->userdata('Error').'
				</div>
		    </div>
			<br>';
			$this->session->unset_userdata('Error');
		}
		$settings = $settings[0];
		?>

    <form action="settings/do_edit_settings" method="post" enctype="multipart/form-data">
      <div class="card-panel">
        <h4>Settings</h4>

        <!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="title" name="title" type="text" class="validate" value="<?=$settings->title?>">
              <label for="title">Title (Website Name)</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
        <!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="metaKeyword" name="metaKeyword" type="text" class="validate" value="<?=$settings->metaKeyword?>">
              <label for="metaKeyword">Meta Keyword</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->

        <!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="metaDescription" name="metaDescription" type="text" class="validate" value="<?=$settings->metaDescription?>">
              <label for="metaDescription">Meta Description</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="emailTo" name="emailTo" type="text" class="validate" value="<?=$settings->emailTo?>">
              <label for="emailTo">Email To</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="emailFrom" name="emailFrom" type="text" class="validate" value="<?=$settings->emailFrom?>">
              <label for="emailFrom">Email From</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="mobile" name="mobile" type="text" class="validate" value="<?=$settings->mobile?>">
              <label for="mobile">Mobile#</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="phone" name="phone" type="text" class="validate" value="<?=$settings->phone?>">
              <label for="phone">Phone#</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="Address" name="address" type="text" class="validate" value="<?=$settings->address?>">
              <label for="Address">Address</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col s6">
            <div class="file-field input-field">
              <input class="file-path validate" type="text" />
              <div class="btn">
                <span>Upload Logo</span>
                <input type="file" name="logo" />
              </div>
            </div>
          </div>
		  <div class="col s6">
            <div class="file-field input-field">
					<?php
						if($settings->logo){
							echo '<br><img src="'.base_url().'assets/upload/'.$settings->logo.'">';
						}
					?>	
            </div>
          </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="headerText" name="headerText" type="text" class="validate" value="<?=$settings->headerText?>">
              <label for="headerText">Header Text</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="footerText" name="footerText" type="text" class="validate" value="<?=$settings->footerText?>">
              <label for="footerText">Footer Text</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="copyright" name="copyright" type="text" class="validate" value="<?=$settings->copyright?>">
              <label for="copyright">Copyright</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
	
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="text_1" name="text_1" type="text" class="validate" value="<?=$settings->text_1?>">
              <label for="text_1">Text 1</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="text_2" name="text_2" type="text" class="validate" value="<?=$settings->text_2?>">
              <label for="text_2">Text 2</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->

		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
              <input id="text_3" name="text_3" type="text" class="validate" value="<?=$settings->text_3?>">
              <label for="text_3">Text 3</label>
            </div>
          </div>
        </div>
        <!-- /Text Field -->
		
		<!-- Text Field -->
        <div class="row">
          <div class="col l2 l12">
            <div class="input-field">
			<button class="waves-effect waves-light btn-large z-depth-0 z-depth-1-hover"  name="btn" value="1">Save</button>          
			</div>
          </div>
        </div>
        <!-- /Text Field -->
		

		
      </div> 

    </form>

  </section>
  <!-- /Main Content -->

		  <!-- Search Bar -->
		  <?=$this->load->view('fyadmin/search_bar');?>	
		  <!-- /Search Bar -->




		<!--Chat-->
		<?=$this->load->view('fyadmin/chat');?>	
		<!-- /Chat -->

  
		<!-- footer -->
		<?=$this->load->view('fyadmin/footer');?>	
		<!-- /.footer -->
  <!-- DEMO [REMOVE IT ON PRODUCTION] -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_demo.js"></script>

  <!-- jQuery -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jquery/jquery.min.js"></script>

  <!-- jQuery RAF (improved animation performance) -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

  <!-- nanoScroller -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

  <!-- Materialize -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/materialize/js/materialize.min.js"></script>

  <!-- Sortable -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/sortable/Sortable.min.js"></script>

  <!-- Main -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/_con/js/_con.min.js"></script>


  <!-- Google Prettify -->
  <script type="text/javascript" src="<?=PATH_ADMIN?>assets/google-code-prettify/prettify.js"></script>
</body>

</html>