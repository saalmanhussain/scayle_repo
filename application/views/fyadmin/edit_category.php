<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $site_Info['Title'] ?></title>

        <meta name="description" content="<?= $site_Info['MetaDescription'] ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>

        <link rel="icon" type="image/png" href="<?= PATH_ADMIN ?>assets/_con/images/icon.png">

        <!-- nanoScroller -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/nanoScroller/nanoscroller.css" />


        <!-- FontAwesome -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/font-awesome/css/font-awesome.min.css" />

        <!-- Material Design Icons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/material-design-icons/css/material-design-icons.min.css" />

        <!-- IonIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/ionicons/css/ionicons.min.css" />

        <!-- WeatherIcons -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/weatherIcons/css/weather-icons.min.css" />

        <!-- Rickshaw -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/rickshaw/rickshaw.min.css" />

        <!-- jvectormap -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/jquery-jvectormap/jquery-jvectormap.css" />

        <!-- Google Prettify -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/google-code-prettify/prettify.css" />
        <!-- Main -->
        <link rel="stylesheet" type="text/css" href="<?= PATH_ADMIN ?>assets/_con/css/_con.min.css" />

        <!--[if lt IE 9]>
          <script src="<?= PATH_ADMIN ?>assets/html5shiv/html5shiv.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <!-- /Top Navbar -->
        <?= $this->load->view('fyadmin/top-navbar'); ?>	
        <!-- /Top Navbar -->

        <!-- Sidebar -->
        <?= $this->load->view('fyadmin/sidebar'); ?>	
        <!-- /.sidebar -->


        <!-- Main Content -->
        <section class="content-wrap">


            <!-- Breadcrumb -->
            <div class="page-title">

                <div class="row">
                    <div class="col s12 m9 l10">
                        <h1>Edit Category</h1>

                        <ul>
                            <li>
                                <a href="<?=base_url()?>fyadmin/dashboard"><i class="fa fa-home"></i> Dashboard</a>  <i class="fa fa-angle-right"></i>
                            </li>

                            <li>
                                <a href='<?=base_url()?>fyadmin/categories/view_category'>Manage Categories</a>  <i class='fa fa-angle-right'></i>
                            </li>
                            <li>
                                <a >Edit Category</a>
                            </li>

                        </ul>
                    </div>
                    <div class="col s12 m3 l2 right-align">
                        <a href="#!" class="btn grey lighten-3 grey-text z-depth-0 chat-toggle"><i class="fa fa-comments"></i></a>
                    </div>
                </div>

            </div>
            <!-- /Breadcrumb -->


            <?php
            if ($this->session->userdata('Success')) {
                echo '
			<div class="alert green lighten-4 green-text text-darken-2">
			' . $this->session->userdata('Success') . '
			</div>
			<br>';
                $this->session->unset_userdata('Success');
            }

            if ($this->session->userdata('Error')) {
                echo '
				<div class="alert">
				  ' . $this->session->userdata('Error') . '
				</div>
		    </div>
			<br>';
                $this->session->unset_userdata('Error');
            }
            ?>



            <!-- With Export Options -->
            <div class="card-panel">

                <form method="POST" enctype="multipart/form-data" data-parsley-validate>

                    <div class="input-field">
                        <i class="mdi-communication-chat prefix"></i>
                        <input id="input_title" type="text" name="title" value="<?= $cat[0]->title ?>" required="">
                        <label for="input_title">Category Title</label>
                    </div>

                    <div class="input-field">
                        <i class="mdi-communication-chat prefix"></i>
                        <textarea id="input_desc" name="description" class="materialize-textarea" required=""><?= $cat[0]->description ?></textarea>
                        <label for="input_desc">Category Description</label>
                    </div>
                    
                    <div class="file-field input-field">
                        <img src="<?= PATH ?>upload/<?= $cat[0]->image ?>" height='200' width='300' />
                        <input type="hidden" name="cat_old_image" value="<?= $cat[0]->image ?>" />
                    </div>
                    
                    <div class="file-field input-field">
                        <input class="file-path validate" type="text" name="cat_image" readonly="" />
                        <div class="btn">
                            <span>Image(Size: W:171 X H:171)</span>
                            <input type="file" id="cat_image" name="cat_image" accept=".jpg,.png,.JPG" />
                        </div>
                    </div>

                    

                    <input type="hidden" value="<?=$catID?>" name="catID">

                    <button name="edit_category" class="waves-effect btn">Update</button>

                </form>
            </div>
            <!-- /With Export Options -->

        </section>
        <!-- /Main Content -->

        <!--Chat-->
        <?= $this->load->view('fyadmin/search_bar'); ?>	
        <!-- /Chat -->

        <!--Chat-->
        <?= $this->load->view('fyadmin/chat'); ?>	
        <!-- /Chat -->
        <!-- /Chat -->

        <!-- footer -->
        <?= $this->load->view('fyadmin/footer'); ?>	
        <!-- /.footer -->

        <!-- DEMO [REMOVE IT ON PRODUCTION] -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_demo.js"></script>

        <!-- jQuery -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery/jquery.min.js"></script>

        <!-- jQuery RAF (improved animation performance) -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jqueryRAF/jquery.requestAnimationFrame.min.js"></script>

        <!-- nanoScroller -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/nanoScroller/jquery.nanoscroller.min.js"></script>

        <!-- Materialize -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/materialize/js/materialize.min.js"></script>


        <!-- Simple Weather -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/simpleWeather/jquery.simpleWeather.min.js"></script>

        <!-- Sparkline -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/sparkline/jquery.sparkline.min.js"></script>

        <!-- Flot -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.time.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.pie.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.tooltip.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/flot/jquery.flot.categories.min.js"></script>

        <!-- d3 -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/d3/d3.min.js"></script>

        <!-- Rickshaw -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/rickshaw/rickshaw.min.js"></script>

        <!-- jvectormap -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery-jvectormap/jquery-jvectormap.min.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/jquery-jvectormap/gdp-data.js"></script>
        <!-- Sortable -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/sortable/Sortable.min.js"></script>

        <!-- Main -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/_con/js/_con.min.js"></script>


        <!-- Google Prettify -->
        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/google-code-prettify/prettify.js"></script>

        <script type="text/javascript" src="<?= PATH_ADMIN ?>assets/parsley/parsley.min.js"></script>

        <script>

            /* Title file  */

            var file2 = document.getElementById('cat_image');
            file2.onchange = function (e) {

                //$("#cat_image_error").html('');
                //$("#cat_image_error").hide();

                var file = this.files[0];

                if ('size' in file) {
                    if (file.size > 2000000)
                    {
                        alert('Error: File Size Error.');
                        //$("#cat_image_error").show();

                        this.value = '';
                        $("[name='cat_image']").focus();
                        return false;
                    }
                }

                var ext = this.value.match(/\.([^\.]+)$/)[1];
                switch (ext)
                {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                        break;
                    default:

                        alert('Error: Incorrect format. Only jpg,jpeg,png are allowed.');
                        //$("#cat_image_error").show();

                        this.value = '';
                        $("[name='cat_image']").focus();
                }
            };
        </script>
    </body>
</html>