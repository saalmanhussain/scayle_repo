<header id="masthead" class="site-header" role="banner">
				<div class="container_12">

					<!-- Site title and description -->
					<div class="grid_6 branding"> 
						<div id="logo" class="logo fleft">
							<img src="<?=PATH?>images/logo.png" alt="Logo" width="60" height="60">
						</div>
						<div class="fleft">
							<h1 id="site-title"><a href="<?=base_url()?>" title="FitnessLife" rel="home"><?=$site_Info['Title']?></a></h1>
							<h2 id="site-description"><?=$site_Info['HeaderText']?></h2>
						</div>
					</div>
					<div class="grid_6 no-m-t lang">
						<ul class="language-menu right">
							<li></li>
							<li></li>
						</ul>
						<div class="clear"></div>
						<div class="callus"><a href="tel:+<?=$site_Info['Phone']?>">Call Us: <?=$site_Info['Phone']?></a></div>
					</div>
					<div class="clear"></div>
				</div>

				<!-- Site Navigation -->
				<div class="main-menu-section">
					<div class="container_12">
						<nav class="site-navigation main-navigation" role="navigation">
							<ul class="menu">
								<?php echo $this->menu->get_menu();  ?>
							</ul>
						</nav>

						<!-- Search Form -->
						
					</div>
				</div>

			</header>