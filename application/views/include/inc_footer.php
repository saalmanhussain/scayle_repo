<footer id="colophon" class="site-footer" role="contentinfo">

			<!-- Footer Widget Area -->
			<div class="widget-area container_12" role="complementary">

				<aside class="widget grid_4">
					<div class="widget-header">
						<h3 class="title">Contact Us</h3>
					</div>
					<address class="vcard">
						<span><?=$site_Info['Address']?></span>
						<br/>
						<span>Phone: +<?=$site_Info['Phone']?></span>
						<br/>
						<span>Mobile: +<?=$site_Info['Mobile']?></span>
						<br/>
						<span>E-mail: <?=$site_Info['EmailTo']?></span>
						<br/>
						<span>Website: <?=base_url()?></span>
					</address>
				</aside>

				<aside class="widget grid_4">
					<div class="widget-header">
						<h3 class="title">Social Networks</h3>
					</div>
					<div class="share-items">
						<?php if($site_Info['Facebook']){?>
							<a href="<?=$site_Info['Facebook']?>" target="_blank" class="icon-facebook"></a>
						<?php }?>
						<?php if($site_Info['Twitter']){?>
							<a href="<?=$site_Info['Twitter']?>" target="_blank" class="icon-twitter"></a>
						<?php }?>
						<?php if($site_Info['GooglePlus']){?>
							<a href="<?=$site_Info['GooglePlus']?>" target="_blank" class="icon-google-plus"></a>
						<?php }?>
						<?php if($site_Info['LinkedIn']){?>
							<a href="<?=$site_Info['LinkedIn']?>" target="_blank" class="icon-linkedin"></a>
						<?php }?>
					</div>
				</aside>

				<aside class="widget grid_4">
					<div class="widget-header">
						<h3 class="title">Business Hours</h3>
					</div>
					<ul>
						<li><span>Sunday <b>06:00 - 22:00</b></span></li>
						<li><span>Wednesday <b>06:30 - 21:00</b></span></li>
						<li><span>Sunday <b>06:00 - 21:00</b></span></li>
					</ul>
				</aside>

			</div>

			<!-- Footer Navigation -->
			<div class="container_12">
				<nav class="grid_12 no-m-b footer-navigation">
					<ul>
						<?php echo $this->menu->get_menu();  ?>
						<li class="go-top"><a title="Go top" href="#" class="icon-arrow-up4"></a></li>
					</ul>
				</nav> 
				<div class="clear"></div>
			</div>

			<!-- Site Info -->
			<div class="site-info">
				<div class="container_12">
					<div class="grid_6">
						<span class="theme-copyrights"><?=$site_Info['Copyright']?></span>
					</div>
					<div class="grid_6">
						<span class="theme-author right"><?=$site_Info['FooterText']?></span>
					</div>
					<div class="clear"></div>
				</div>
			</div>

		</footer>